﻿using FGC.Data.Base;

namespace FGC.CoreData
{
    public class MessagesLanguageDetail : BaseEntity
    {
        public MessagesLanguageDetail()
        {
        }

        public int ID { get; set; }  
        public string Message { get; set; }
        public string LanguageCode { get; set; }
        public int LanguageID { get; set; } 
        public int ViewModelMessageID { get; set; } 

    }
}