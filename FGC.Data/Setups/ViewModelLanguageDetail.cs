﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("ViewModelLanguageDetail")]
    public class ViewModelLanguageDetail : BaseEntity
    {
        public ViewModelLanguageDetail()
        { 
        }
        public int ID { get; set; }
        //[ForeignKey("ViewModelDetail")]
        public int ViewModeDetailID { get; set; }

        public string DatabaselName { get; set; }
        public string FieldLabelName { get; set; }
        public string Placeholder { get; set; } 
        public string ValidationMessage { get; set; } 
        public string LanguageCode { get; set; } 
        public string Help { get; set; }
        //public virtual ViewModelDetail ViewModelDetail { get; set; }
           
    }
}
