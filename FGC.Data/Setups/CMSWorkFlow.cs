﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    public class CMSWorkFlow
    {
        public CMSWorkFlow()
        {

        }
        public int ID { get; set; }

        [Required]
        public int EntityID { get; set; }
        public int StatusID { get; set; }

        public int RecordID { get; set; }
        public DateTime PublishDate { get; set; }
        
        public string EntityName { get; set; } 

    }
}
