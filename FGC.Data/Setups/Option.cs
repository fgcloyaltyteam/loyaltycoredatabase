﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("Option")]
    public class Option : BaseEntity
    {
        public Option()
        {
            IsDefault = false;
        }

        [Key]
        public int ID { get; set; }
     
        /// <summary>
        /// Option Name, that is visible to user
        /// </summary>
        [Required]
        public string Name { get; set; }
        public int ParentID { get; set; }
        public string Description { get; set; }

        [Required]
        public int SubModuleID { get; set; }

        public bool IsDefault { get; set; }

    }
}
 
