﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.CoreData
{
    public class Module : BaseEntity
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }

    }
}
