﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.Data.Client
{
    public class PartialViewModel
    {
        public PartialViewModel()
        {
            //Module = new Module();
        }
        public int Id { get; set; }
        [Required]
        public string ViewName { get; set; }
        public string ViewCSS { get; set; }
  
        [Required]
        [ForeignKey("ViewModel")]
        public int ViewModelId { get; set; }

        public virtual ViewModel ViewModel { get; set; }

    }
}
