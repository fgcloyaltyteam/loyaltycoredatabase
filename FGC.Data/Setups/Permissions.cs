﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    public class Permissions
    {
        public Permissions()
        {
           // Module = new Module();
        }
        public int ID { get; set; }

        public int MemberType { get; set; }
        public int MemberID { get; set; }
        public int ProgramWebsiteID { get; set; }
        public int MenuID { get; set; } 
        public bool CanRead { get; set; }
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanApprove { get; set; }
        public bool IsActive { get; set; }

    }
}
