﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    public class ViewModelTracking
    {
        public ViewModelTracking()
        {
            // Module = new Module();
        }
        public int ID { get; set; }

        [Required]
        public int ViewModelD { get; set; }
        public int ViewModelDetailID { get; set; }

        public string FieldValue { get; set; }
        public int MemberID { get; set; }
        public bool IsProcessed { get; set; }

    }
}
