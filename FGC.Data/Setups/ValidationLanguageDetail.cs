﻿using FGC.Data.Base;

namespace FGC.CoreData
{
    public class ValidationLanguageDetail : BaseEntity
    {
        public ValidationLanguageDetail()
        {
        }

        public int ID { get; set; } 
        //[ForeignKey("ViewModelDetail")]
        public int ViewModeDetailID { get; set; } 
        public string ValidationMessage { get; set; }
        public string LanguageCode { get; set; }
        public int LanguageID { get; set; } 
        public int ViewModelValidationID { get; set; } 

    }
}