﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("Menu")]
    public class Menu : BaseEntity
    {
        public Menu()
        {
            // Module = new Module();
            HideAfterLogin = false;
            MenuPosition = 1;  // 1 = Top , 2 = Bottom, 3 = Left, 4 = Right
            LoginRequired = true;
        }

        [Key]
        public int ID { get; set; }

        public int CoreMenuID { get; set; }
 
        public int PortalID { get; set; }

        [Required]
        public int ServiceID { get; set; }

        public int ContentTypeID { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string ControllerName { get; set; }

        [Required]
        public string ActionName { get; set; }

        [Required]
        public int ParentID { get; set; }

        [Required]
        public int MenuPosition { get; set; }

        [Required]
        public bool LoginRequired { get; set; }

        [Required]
        public bool HideAfterLogin { get; set; }

        [Required]
        public int Sequence { get; set; }

        public string CssClass { get; set; }
        public string MetaKeyword { get; set; }
        public int ModuleID  { get; set; }

    }
}
