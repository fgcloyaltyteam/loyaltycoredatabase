﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("OptiViewModelon")]
    public class ViewModel
    {
        public ViewModel()
        {

        }
        public int ID { get; set; }

        [Required]
        public string ViewName { get; set; }
        public string ViewCSS { get; set; }

        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }

        public string Entity { get; set; }

        [Required]
        public int ModuleID { get; set; }

        [Required]
        public int MenuID { get; set; }

        public string CMSLink   { get; set; }

    }
}
