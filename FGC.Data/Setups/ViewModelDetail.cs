﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("ViewModelDetail")]
    public class ViewModelDetail //: BaseEntity
    {
        public ViewModelDetail()
        {
            Approved = false;
            Applied = false; 
        }
        public int ID { get; set; }
        public int ViewModelID { get; set; }
        public string ViewName { get; set; }
        public string FieldLabelName { get; set; }
        public string SourceEntity { get; set; } 
        public string Name { get; set; } 
        public bool AllowSearching { get; set; } 
        public bool Display { get; set; } 
        public string ControlType { get; set; }
        public bool? LoadFromJSON { get; set; }
        public int Sequence { get; set; }
        public int SubModuleID { get; set; }
        public bool IsSearchable { get; set; }
        public bool IsActive { get; set; }
        public string UIServingClassName { get; set; }
        public string CSSClassName  { get; set; }
        public string LabelCSSClassName { get; set; }
        public string Placeholder { get; set; }
        public string DropDownJson { get; set; }
        public string ValidationMessage { get; set; }
        public bool? LoadFromDataBase { get; set; }

        //public int LanguageID { get; set; }
        public string LanguageCode { get; set; }
        public bool Approved { get; set; }
        public bool Applied { get; set; }
        public string Help { get; set; }


    }
}
