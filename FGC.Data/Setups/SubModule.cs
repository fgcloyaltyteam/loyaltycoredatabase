﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    public class SubModule : BaseEntity
    {
        public SubModule()
        {
           // Module = new Module();
        }
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required]
       // [ForeignKey("Module")]
        public int ModuleID { get; set; }
       // public virtual Module Module { get; set; }

    }
}
 