﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.Data.Client
{
    public class PackageModule
    {
        public PackageModule()
        {
            Module = new Module();
            Package = new Core.Package();
        }
        public int ID { get; set; }
        

        [Required, ForeignKey("Module")]
        public int ModuleID { get; set; }
        public virtual Module Module { get; set; }


        [Required, ForeignKey("Package")]
        public int PackageID { get; set; }



        public virtual Core.Package Package { get; set; }

    }
}
/*
 
Options
{ 	
ID	 
SubModuleID	 
Name	 
Description	 

}
*/
