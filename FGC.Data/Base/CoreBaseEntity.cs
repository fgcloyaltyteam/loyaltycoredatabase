﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Base
{
    public abstract class CoreBaseEntity
    {
        public CoreBaseEntity()
        {
            DateCreated = DateTime.Now;
            DateModified = DateTime.Now; 
            IsActive = true;
            IsDeleted = false;
            RowID = Guid.NewGuid();
        } 

        public Guid RowID { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public int CreatedBy {get;set;}
        public string ChangeSource {get;set;}  //(Member/Admin)
        public int ModifiedBy { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
