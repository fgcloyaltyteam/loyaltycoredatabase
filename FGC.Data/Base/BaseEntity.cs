﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Base
{
    public abstract class BaseEntity
    {
        public BaseEntity()
        {
            DateCreated = DateTime.Now;
            DateModified = DateTime.Now; 
            IsActive = true;
            IsDeleted = false;
            RowID = Guid.NewGuid();
        }

        [DefaultValue("newid()")]
        public Guid RowID { get; set; }

        [DefaultValue("sysdatetime()")]
        public DateTime DateCreated { get; set; }

        [DefaultValue("sysdatetime()")]
        public DateTime DateModified { get; set; }

        [DefaultValue("1")]
        public int CreatedBy {get;set;}

        [DefaultValue("1")]
        public int ModifiedBy { get; set; }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            

        public string ChangeSource { get; set; }  //(Member/Admin)

        [DefaultValue("1")]
        public bool IsActive { get; set; }

        [DefaultValue("0")]
        public bool IsDeleted { get; set; }
    }
}
