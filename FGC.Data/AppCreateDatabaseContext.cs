﻿using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Common;
//using FGC.Data.Client;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.Entity.ModelConfiguration.Conventions;
using FGC.Data.Core;
using Newtonsoft.Json;
using System.IO;

namespace FGC.Data
{
    public class AppCreateDatabaseContext : DbContext
    {
        public AppCreateDatabaseContext(DbConnection connection, DbCompiledModel model) : base(connection, model, contextOwnsConnection: false)
        {
            Database.SetInitializer<AppCreateDatabaseContext>(null);
        }
         
        private static ConcurrentDictionary<Tuple<string, string>, DbCompiledModel> modelCache
            = new ConcurrentDictionary<Tuple<string, string>, DbCompiledModel>();

        #region DbSets

        public DbSet<Location> Location { get; set; }

        public DbSet<Member> Member { get; set; }
        public DbSet<MemberActivity> MemberActivity { get; set; }
        public DbSet<MemberAddress> MemberAddress { get; set; }
        public DbSet<MemberChangePassword> MemberChangePassword { get; set; }
        public DbSet<MemberCommunicationPreference> MemberCommunicationPreference { get; set; }
        public DbSet<MemberContactInfo> MemberContactInfo { get; set; }
        public DbSet<MemberCurrentStatus> MemberCurrentStatus { get; set; }
        public DbSet<MemberCurrentType> MemberCurrentType { get; set; }
        public DbSet<MemberDetail> MemberDetail { get; set; }
        public DbSet<MemberForgotPassword> MemberForgotPassword { get; set; }
        public DbSet<MemberHouseholdInfo> MemberHouseholdInfo { get; set; }
        public DbSet<MemberPasswordLog> MemberPasswordLog { get; set; }
        public DbSet<MemberPoint> MemberPoint { get; set; }
        public DbSet<MemberProfile> MemberProfile { get; set; }
        public DbSet<MemberProgram> MemberProgram { get; set; }
        public DbSet<MemberRegion> MemberRegion { get; set; }
        public DbSet<MemberRegistration> MemberRegistration { get; set; }
        public DbSet<MemberSalesAdvisor> MemberSalesAdvisor { get; set; }
        public DbSet<MemberSecurityQuestion> MemberSecurityQuestion { get; set; }
        public DbSet<MemberStatusCycle> MemberStatusCycle { get; set; }
        //public DbSet<MemberStatusMaster> MemberStatusMaster { get; set; }
        public DbSet<MemberTerritory> MemberTerritory { get; set; }
        //public DbSet<MemberType> MemberType { get; set; }
        public DbSet<MemberTypeCycle> MemberTypeCycle { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<SecurityQuestion> SecurityQuestion { get; set; }
        public DbSet<Territory> Territory { get; set; }
        public DbSet<MemberProfileViewData> MemberProfileViewData { get; set; }

        //<----------- Offer ------->
        // -- Purchase -- //
        public DbSet<PurchaseOffer> PurchaseOffer { get; set; }
        public DbSet<PurchaseOfferLocation> PurchaseOfferLocation { get; set; }
        public DbSet<PurchaseOfferMember> PurchaseOfferMember { get; set; }
        public DbSet<PurchaseOfferProduct> PurchaseOfferProduct { get; set; }

        // -- Redemption -- //
        public DbSet<RedemptionOffer> RedemptionOffer { get; set; }
        public DbSet<RedemptionOfferProduct> RedemptionOfferProduct { get; set; }

        //<----------- Promotion ------->
        // -- Purchase -- //
        public DbSet<PurchasePromotion> PurchasePromotion { get; set; }
        public DbSet<PurchasePromotionLocation> PurchasePromotionLocation { get; set; }
        public DbSet<PurchasePromotionMember> PurchasePromotionMember { get; set; }
        public DbSet<PurchasePromotionProduct> PurchasePromotionProduct { get; set; }


        //<-----------  Order -> Common ------->
        public DbSet<ChargeType> ChargeType { get; set; }

        //<-----------  Order -> Purchase Order ------->
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderBillingInfo> OrderBillingInfo { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        public DbSet<OrderDiscount> OrderDiscount { get; set; }
        public DbSet<OrderPaymentInfo> OrderPaymentInfo { get; set; }
        public DbSet<OrderPaymentSummary> OrderPaymentSummary { get; set; }
        public DbSet<OrderShippingCharge> OrderShippingCharge { get; set; }
        public DbSet<OrderShippingInfo> OrderShippingInfo { get; set; }
        public DbSet<OrderTax> OrderTax { get; set; }
        public DbSet<ReferenceOrder> ReferenceOrder { get; set; }
        public DbSet<ReferenceOrderDetail> ReferenceOrderDetail { get; set; }



        // Order -> Redemption Order
        public DbSet<RedemptionOrder> RedemptionOrder { get; set; }
        public DbSet<RedemptionOrderBillingInfo> RedemptionOrderBillingInfo { get; set; }
        public DbSet<RedemptionOrderDetail> RedemptionOrderDetail { get; set; }
        public DbSet<RedemptionOrderPaymentInfo> RedemptionOrderPaymentInfo { get; set; }
        public DbSet<RedemptionOrderShippingCharge> RedemptionOrderShippingCharge { get; set; }
        public DbSet<RedemptionOrderShippingInfo> RedemptionOrderShippingInfo { get; set; }
        public DbSet<ReferenceRedemptionOrder> ReferenceRedemptionOrder { get; set; }
        public DbSet<ReferenceRedemptionOrderDetail> ReferenceRedemptionOrderDetail { get; set; }


        // Product Common
        public DbSet<Brand> Brand { get; set; }
        public DbSet<BrandCategory> BrandCategory { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Property> Property { get; set; }
        public DbSet<PropertyValue> PropertyValue { get; set; }

        // Product - Purchase Products
        public DbSet<PurchaseCatalog> PurchaseCatalog { get; set; }
        public DbSet<PurchaseCatalogProduct> PurchaseCatalogProduct { get; set; }
        public DbSet<PurchaseProduct> PurchaseProduct { get; set; }
        public DbSet<PurchaseProductVariant> PurchaseProductVariant { get; set; }
        public DbSet<PurchaseProductCategory> PurchaseProductCategory { get; set; }
        public DbSet<PurchaseProductImage> PurchaseProductImage { get; set; }
        public DbSet<PurchaseProductProperty> PurchaseProductProperty { get; set; }

        public DbSet<ServingProductByCatalog> ServingProductByCatalog { get; set; }
        public DbSet<ServingProductByCategory> ServingProductByCategory { get; set; }
        public DbSet<ServingProductImage> ServingProductImage { get; set; }
        public DbSet<ServingProductList> ServingProductList { get; set; }
        public DbSet<ServingProductVariant> ServingProductVariant { get; set; }

        // Product - Redemption Products
        public DbSet<RedemptionCatalog> RedemptionCatalog { get; set; }
        public DbSet<RedemptionCatalogProduct> RedemptionCatalogProduct { get; set; }
        public DbSet<RedemptionProduct> RedemptionProduct { get; set; }
        public DbSet<RedemptionProductCategory> RedemptionProductCategory { get; set; }
        public DbSet<RedemptionProductImage> RedemptionProductImage { get; set; }
        public DbSet<RedemptionProductOtherInfo> RedemptionProductOtherInfo { get; set; }
        public DbSet<RedemptionProductProperty> RedemptionProductProperty { get; set; }
        public DbSet<RedemptionProductVariant> RedemptionProductVariant { get; set; }

        public DbSet<ServingRedemptionProductByCatalog> ServingRedemptionProductByCatalog { get; set; }
        public DbSet<ServingRedemptionProductByCategory> ServingRedemptionProductByCategory { get; set; }
        public DbSet<ServingRedemptionProductImage> ServingRedemptionProductImage { get; set; }
        public DbSet<ServingRedemptionProductList> ServingRedemptionProductList { get; set; }
        public DbSet<ServingRedemptionProductVariant> ServingRedemptionProductVariant { get; set; }

        public DbSet<RedemptionCartLog> RedemptionCartLog { get; set; }


        // Client
        public DbSet<Entities> Entities { get; set; }
        public DbSet<ProcessLog> ProcessLog { get; set; }
        public DbSet<RouteConfiguration> RouteConfiguration { get; set; }


        // Setup
        public DbSet<ContentDetail> ContentDetail { get; set; }
        public DbSet<Module> Module { get; set; }
        public DbSet<Option> Option { get; set; }
        public DbSet<SubModule> SubModule { get; set; }
        public DbSet<ViewModel> ViewModel { get; set; }
        public DbSet<ViewModelDetail> ViewModelDetail { get; set; }
        public DbSet<ViewModelLanguageDetail> ViewModelLanguageDetail { get; set; }
        public DbSet<MenulLanguageDetail> MenulLanguageDetail { get; set; }
        public DbSet<ViewModelTracking> ViewModelTracking { get; set; }
        public DbSet<ViewModelValidation> ViewModelValidation { get; set; }
        public DbSet<ValidationLanguageDetail> ValidationLanguageDetail { get; set; }
        public DbSet<ViewModelMessages> ViewModelMessages { get; set; }
        public DbSet<MessagesLanguageDetail> MessagesLanguageDetail { get; set; }

        public DbSet<CMSWorkFlow> CMSWorkFlow { get; set; }
        public DbSet<Permissions> Permissions { get; set; }
        public DbSet<MemberPortal> MemberPortal { get; set; }
        public DbSet<MemberType> MemberType { get; set; }
        public DbSet<Menu> Menu { get; set; }

        // FAQ
        public DbSet<FAQ> FAQ { get; set; }
        public DbSet<FAQAnswer> FAQAnswer { get; set; }

        // Static pages
        public DbSet<StaticPage> StaticPage { get; set; }

        // CSS Theme
        public DbSet<CSSLayout> CSSLayout { get; set; }
        public DbSet<CSSRepositoryThemeVariableValue> CSSRepositoryThemeVariableValue { get; set; }
        public DbSet<CSSTheme> CSSTheme { get; set; }
        public DbSet<CSSThemeRepository> CSSThemeRepository { get; set; }
        public DbSet<CSSVariable> CSSVariable { get; set; }
        public DbSet<CSSVariableDefaultValue> CSSVariableDefaultValue { get; set; }
        public DbSet<CSSThemeVariableValue> CSSThemeVariableValue { get; set; }
        public DbSet<CSSWebsiteTheme> CSSWebsiteTheme { get; set; }
        public DbSet<Ticket> Ticket { get; set; }
        public DbSet<TicketComment> TicketComment { get; set; }
        public DbSet<TicketDocument> TicketDocument { get; set; }


        // Reports Configuration
        public DbSet<ControlType> ControlType { get; set; }
        public DbSet<Report> Report { get; set; }
        public DbSet<ReportGroup> ReportGroup { get; set; }
        public DbSet<ReportSubGroup> ReportSubGroup { get; set; }
        public DbSet<ReportTable> ReportTable { get; set; }
        public DbSet<ReportTableColumn> ReportTableColumn { get; set; }

        // Reports 
        public DbSet<MemberPurchaseSummaryByYearMonth> MemberPurchaseSummaryByYearMonth { get; set; }




        #endregion

        public static AppCreateDatabaseContext Create(Guid schema, DbConnection connection)
        {

            var compiledModel = modelCache.GetOrAdd(
                Tuple.Create(connection.ConnectionString, schema.ToString()),
                t =>
                {
                    try
                    {
                        var builder = new DbModelBuilder();

                        #region Entity Configuration
                        // Location
                        builder.Entity<Location>().Property(x => x.AddressLine1).IsOptional();
                        builder.Entity<Location>().Property(x => x.AddressLine2).IsOptional();
                        builder.Entity<Location>().Property(x => x.AddressLine3).IsOptional();
                        builder.Entity<Location>().Property(x => x.City).IsOptional();
                        builder.Entity<Location>().Property(x => x.PostalCode).IsOptional();
                        builder.Entity<Location>().Property(x => x.ProvinceID).IsOptional();
                        builder.Entity<Location>().Property(x => x.CountryID).IsOptional();

                        // Client ->  Member
                        builder.Entity<Member>().Property(x => x.ClientMemberID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.RefTransactionID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.ShipDate).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.RefMemberID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.PurchaseAmount).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.Price).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.Cost).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.TransactionComment).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.WholesalerID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.MerchantID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.ChannelID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.LocationID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.SalesRepID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.CategoryID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.SourceID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.ReasonID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.OfferID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.TokenID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.OrderID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.ProductID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.ProductSerialNumber).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.UserComment).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.DataSource).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.PartnerMerchantID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.BrandMerchantID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.TerminalID).IsOptional();
                        builder.Entity<MemberActivity>().Property(x => x.FileID).IsOptional();


                        builder.Entity<MemberAddress>().Property(x => x.AddressLine2).IsOptional();
                        builder.Entity<MemberAddress>().Property(x => x.AddressLine3).IsOptional();

                        builder.Entity<MemberCommunicationPreference>().Property(x => x.IsShowSurvey).IsOptional();
                        builder.Entity<MemberCommunicationPreference>().Property(x => x.CommunicateViaEmail).IsOptional();
                        builder.Entity<MemberCommunicationPreference>().Property(x => x.CommunicateViaSms).IsOptional();
                        builder.Entity<MemberCommunicationPreference>().Property(x => x.ReceiveNotification).IsOptional();
                        builder.Entity<MemberCommunicationPreference>().Property(x => x.ShowYourInformation).IsOptional();

                        builder.Entity<MemberContactInfo>().Property(x => x.CompanyName).IsOptional();
                        builder.Entity<MemberContactInfo>().Property(x => x.AlternateEmailAddress).IsOptional();
                        builder.Entity<MemberContactInfo>().Property(x => x.AlternatePhone).IsOptional();
                        builder.Entity<MemberContactInfo>().Property(x => x.Mobile).IsOptional();
                        builder.Entity<MemberContactInfo>().Property(x => x.Fax).IsOptional();

                        builder.Entity<MemberDetail>().Property(x => x.ImageFileName).IsOptional();
                        builder.Entity<MemberDetail>().Property(x => x.CategoryID).IsOptional();
                        builder.Entity<MemberDetail>().Property(x => x.MemberNotes).IsOptional();
                        builder.Entity<MemberDetail>().Property(x => x.HowDidYouHearID).IsOptional();

                        builder.Entity<MemberForgotPassword>().Property(x => x.MemberID).IsOptional();
                        builder.Entity<MemberForgotPassword>().Property(x => x.Notes).IsOptional();

                        builder.Entity<MemberHouseholdInfo>().Property(x => x.MaritalStatusID).IsOptional();
                        builder.Entity<MemberHouseholdInfo>().Property(x => x.NoOfFamilyMembers).IsOptional();
                        builder.Entity<MemberHouseholdInfo>().Property(x => x.NoOfMaleMembers).IsOptional();
                        builder.Entity<MemberHouseholdInfo>().Property(x => x.NoOfFemaleMembers).IsOptional();
                        builder.Entity<MemberHouseholdInfo>().Property(x => x.NoOfKids).IsOptional();
                        builder.Entity<MemberHouseholdInfo>().Property(x => x.IncomeID).IsOptional();

                        builder.Entity<MemberProfile>().Property(x => x.MiddleInitial).IsOptional();
                        builder.Entity<MemberProfile>().Property(x => x.DOB).IsOptional();
                        builder.Entity<MemberPortal>().Property(x => x.MemberID).IsOptional();

                        builder.Entity<MemberRegistration>().Property(x => x.DeletedDate).IsOptional();
                        builder.Entity<MemberRegistration>().Property(x => x.ApprovalDate).IsOptional();
                        builder.Entity<MemberRegistration>().Property(x => x.DeactivationDate).IsOptional();
                        builder.Entity<MemberRegistration>().Property(x => x.RenewedDate).IsOptional();
                        builder.Entity<MemberRegistration>().Property(x => x.ActivationDate).IsOptional();

                        builder.Entity<MemberStatusCycle>().Property(x => x.Notes).IsOptional();

                        // Purchase Offer
                        builder.Entity<PurchaseOffer>().Property(x => x.DateApproved).IsOptional();
                        builder.Entity<PurchaseOffer>().Property(x => x.ApprovedBy).IsOptional();
                        builder.Entity<PurchaseOffer>().Property(x => x.ModifyReason).IsOptional();

                        builder.Entity<PurchaseOfferLocation>().Property(x => x.DateApproved).IsOptional();
                        builder.Entity<PurchaseOfferLocation>().Property(x => x.ApprovedBy).IsOptional();

                        // Purchase Promotion
                        builder.Entity<PurchasePromotion>().Property(x => x.Description).IsOptional();
                        builder.Entity<PurchasePromotion>().Property(x => x.DateApproved).IsOptional();
                        builder.Entity<PurchasePromotion>().Property(x => x.ApprovedBy).IsOptional();
                        builder.Entity<PurchasePromotion>().Property(x => x.ModifyReason).IsOptional();

                        builder.Entity<PurchasePromotionLocation>().Property(x => x.DateApproved).IsOptional();
                        builder.Entity<PurchasePromotionLocation>().Property(x => x.ApprovedBy).IsOptional();

                        // Redemption Offer
                        builder.Entity<RedemptionOffer>().Property(x => x.Description).IsOptional();
                        builder.Entity<RedemptionOffer>().Property(x => x.DateApproved).IsOptional();
                        builder.Entity<RedemptionOffer>().Property(x => x.ApprovedBy).IsOptional();
                        builder.Entity<RedemptionOffer>().Property(x => x.ModifyReason).IsOptional();

                        // Redemption Promotion



                        // Client -> Order -> Common
                        builder.Entity<ChargeType>().Property(x => x.Description).IsOptional();

                        // Client ->  Order -> Purchase Order
                        builder.Entity<Order>().Property(x => x.cartID).IsOptional();
                        builder.Entity<Order>().Property(x => x.ChannelID).IsOptional();
                        builder.Entity<Order>().Property(x => x.IsMultipleShippers).IsOptional();
                        builder.Entity<Order>().Property(x => x.OrderNumber).IsOptional();
                        builder.Entity<Order>().Property(x => x.TypeID).IsOptional();
                        builder.Entity<Order>().Property(x => x.StatusID).IsOptional();
                        builder.Entity<Order>().Property(x => x.SendEmailStatus).IsOptional();
                        builder.Entity<Order>().Property(x => x.BillingShippingSame).IsOptional();
                        builder.Entity<Order>().Property(x => x.partnerOrderNumber).IsOptional();
                        builder.Entity<Order>().Property(x => x.Comments).IsOptional();
                        builder.Entity<Order>().Property(x => x.PublicComments).IsOptional();
                        builder.Entity<Order>().Property(x => x.salesRepCode).IsOptional();
                        builder.Entity<Order>().Property(x => x.assignedSalesRepCode).IsOptional();
                        builder.Entity<Order>().Property(x => x.orderLocation).IsOptional();
                        builder.Entity<Order>().Property(x => x.AffiliateCameFrom).IsOptional();
                        builder.Entity<Order>().Property(x => x.TrafficSource).IsOptional();
                        builder.Entity<Order>().Property(x => x.TrafficSearchPhrase).IsOptional();
                        builder.Entity<Order>().Property(x => x.PayPerClickOrder).IsOptional();
                        builder.Entity<Order>().Property(x => x.PayPerClickKeywordUsed).IsOptional();
                        builder.Entity<Order>().Property(x => x.GrossAmount).IsOptional();
                        builder.Entity<Order>().Property(x => x.AmountPaid).IsOptional();
                        builder.Entity<Order>().Property(x => x.PONumber).IsOptional();
                        builder.Entity<Order>().Property(x => x.POPaid).IsOptional();
                        builder.Entity<Order>().Property(x => x.TPInvoiceNumber).IsOptional();
                        builder.Entity<Order>().Property(x => x.TPInvoiceDate).IsOptional();
                        builder.Entity<Order>().Property(x => x.LanguageID).IsOptional();
                        builder.Entity<Order>().Property(x => x.BonusGiftID).IsOptional();
                        builder.Entity<Order>().Property(x => x.QRCode).IsOptional();

                        builder.Entity<OrderBillingInfo>().Property(x => x.BillingCompanyName).IsOptional();
                        builder.Entity<OrderBillingInfo>().Property(x => x.BillingAddressLine2).IsOptional();
                        builder.Entity<OrderBillingInfo>().Property(x => x.BillingAddressLine3).IsOptional();
                        builder.Entity<OrderBillingInfo>().Property(x => x.BillingPersonPhone).IsOptional();
                        builder.Entity<OrderBillingInfo>().Property(x => x.BillingPersonEmail).IsOptional();

                        builder.Entity<OrderDetail>().Property(x => x.ItemTypeID).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.ItemQuantityMultiplier).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.NoTaxFlag).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.shippingMethodID).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.ShippingDate).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.PickupDate).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.DeliveryDate).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.SignatureRequired).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.FulfillmentCenterID).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.CarrierID).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.TrackingNo).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.Trackinglink).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.IsGift).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.GiftWrap).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.GiftMessage).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.ShippingInstruction).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.packagingOptionID).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.ShipmentNotification).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.ItemQuantityShipped).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.ItemTaxableAmount).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.ItemTaxAmount).IsOptional();
                        builder.Entity<OrderDetail>().Property(x => x.HBR).IsOptional();


                        builder.Entity<OrderPaymentInfo>().Property(x => x.CurrencyID).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.TypeID).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.GatewayID).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.GatewayName).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.ApprovedPaymentTotal).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.AuthorizationCode).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.CCTypeID).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.CCNumber).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.CCExpMonth).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.CCExpYear).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.CCCVV).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.CCName).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.CCAVSResponse).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.Method).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.RejectReasonCode).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.RejectReasonMessage).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.StatusID).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.ChequeNumber).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.ChequeDate).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.ChequeAmount).IsOptional();
                        builder.Entity<OrderPaymentInfo>().Property(x => x.POAmount).IsOptional();


                        builder.Entity<OrderPaymentSummary>().Property(x => x.ShippingAmount).IsOptional();
                        builder.Entity<OrderPaymentSummary>().Property(x => x.DiscountAmount).IsOptional();
                        builder.Entity<OrderPaymentSummary>().Property(x => x.MemberCredit).IsOptional();
                        builder.Entity<OrderPaymentSummary>().Property(x => x.TaxableAmount).IsOptional();
                        builder.Entity<OrderPaymentSummary>().Property(x => x.TaxAmount).IsOptional();
                        builder.Entity<OrderPaymentSummary>().Property(x => x.OtherCharge).IsOptional();
                        builder.Entity<OrderPaymentSummary>().Property(x => x.AdditionalFees).IsOptional();


                        builder.Entity<OrderShippingInfo>().Property(x => x.ShippingCompanyName).IsOptional();
                        builder.Entity<OrderShippingInfo>().Property(x => x.ShippingAddressLine2).IsOptional();
                        builder.Entity<OrderShippingInfo>().Property(x => x.ShippingAddressLine3).IsOptional();
                        builder.Entity<OrderShippingInfo>().Property(x => x.ContactPersonPhone).IsOptional();
                        builder.Entity<OrderShippingInfo>().Property(x => x.ContactPersonEmail).IsOptional();
                        builder.Entity<OrderShippingInfo>().Property(x => x.TotalWeight).IsOptional();
                        builder.Entity<OrderShippingInfo>().Property(x => x.Instructions).IsOptional();

                        //builder.Entity<ServingOrderDetail>().Property(x => x.).IsOptional();
                        //builder.Entity<ServingOrderList>().Property(x => x.).IsOptional();

                        // Client ->  Order -> Redemption Order
                        builder.Entity<RedemptionOrder>().Property(x => x.cartID).IsOptional();
                        builder.Entity<RedemptionOrder>().Property(x => x.IsPartialShipmentAllowed).IsOptional();
                        //builder.Entity<RedemptionOrder>().Property(x => x.Comments).IsOptional();
                        //builder.Entity<RedemptionOrder>().Property(x => x.PublicComments).IsOptional();
                        builder.Entity<RedemptionOrder>().Property(x => x.QRCode).IsOptional();

                        builder.Entity<RedemptionOrderBillingInfo>().Property(x => x.BillingAddressLine2).IsOptional();
                        builder.Entity<RedemptionOrderBillingInfo>().Property(x => x.BillingAddressLine3).IsOptional();
                        builder.Entity<RedemptionOrderBillingInfo>().Property(x => x.BillingPersonPhone).IsOptional();
                        builder.Entity<RedemptionOrderBillingInfo>().Property(x => x.BillingPersonEmail).IsOptional();

                        builder.Entity<RedemptionOrderDetail>().Property(x => x.ShippingMethodID).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.ShippingStatusID).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.ShippingDate).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.PickupDate).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.DeliveryDate).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.FulfillmentID).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.CarrierID).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.TrackingNo).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.Trackinglink).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.IsGift).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.GiftWrap).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.GiftMessage).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.ShippingInstruction).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.PackagingOptionID).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.ShipmentNotification).IsOptional();
                        builder.Entity<RedemptionOrderDetail>().Property(x => x.QuantityShipped).IsOptional();

                        builder.Entity<RedemptionOrderPaymentInfo>().Property(x => x.DiscountCode).IsOptional();
                        builder.Entity<RedemptionOrderPaymentInfo>().Property(x => x.Discount).IsOptional();
                        builder.Entity<RedemptionOrderPaymentInfo>().Property(x => x.AmountPaid).IsOptional();

                        builder.Entity<RedemptionOrderShippingInfo>().Property(x => x.ShippingCompanyName).IsOptional();
                        builder.Entity<RedemptionOrderShippingInfo>().Property(x => x.ShippingAddressLine2).IsOptional();
                        builder.Entity<RedemptionOrderShippingInfo>().Property(x => x.ShippingAddressLine3).IsOptional();
                        builder.Entity<RedemptionOrderShippingInfo>().Property(x => x.ContactPersonPhone).IsOptional();
                        builder.Entity<RedemptionOrderShippingInfo>().Property(x => x.ContactPersonEmail).IsOptional();
                        builder.Entity<RedemptionOrderShippingInfo>().Property(x => x.TotalWeight).IsOptional();
                        builder.Entity<RedemptionOrderShippingInfo>().Property(x => x.Instructions).IsOptional();


                        // Product - Common
                        builder.Entity<Brand>().Property(x => x.Description).IsOptional();
                        builder.Entity<Category>().Property(x => x.Description).IsOptional();
                        builder.Entity<RedemptionCartLog>().Property(x => x.MemberID).IsOptional();

                        // Purchase Catalog
                        builder.Entity<PurchaseCatalog>().Property(x => x.Description).IsOptional();

                        builder.Entity<PurchaseProduct>().Property(x => x.Description).IsOptional();
                        builder.Entity<PurchaseProduct>().Property(x => x.Price).IsOptional();
                        builder.Entity<PurchaseProduct>().Property(x => x.Cost).IsOptional();
                        builder.Entity<PurchaseProduct>().Property(x => x.SKUCode).IsOptional();
                        builder.Entity<PurchaseProduct>().Property(x => x.UPCCode).IsOptional();
                        builder.Entity<PurchaseProduct>().Property(x => x.GTIN).IsOptional();
                        builder.Entity<PurchaseProduct>().Property(x => x.BaseRewards).IsOptional();

                        builder.Entity<PurchaseProductVariant>().Property(x => x.Description).IsOptional();
                        builder.Entity<PurchaseProductVariant>().Property(x => x.SKUCode).IsOptional();
                        builder.Entity<PurchaseProductVariant>().Property(x => x.UPCCode).IsOptional();
                        builder.Entity<PurchaseProductVariant>().Property(x => x.GTIN).IsOptional();
                        builder.Entity<PurchaseProductVariant>().Property(x => x.Price).IsOptional();
                        builder.Entity<PurchaseProductVariant>().Property(x => x.Cost).IsOptional();

                        builder.Entity<ServingProductList>().Property(x => x.BaseRewards).IsOptional();
                        builder.Entity<ServingProductList>().Property(x => x.ImageID).IsOptional();
                        builder.Entity<ServingProductList>().Property(x => x.ImageName).IsOptional();

                        builder.Entity<ServingProductVariant>().Property(x => x.Description).IsOptional();
                        builder.Entity<ServingProductVariant>().Property(x => x.Price).IsOptional();
                        builder.Entity<ServingProductVariant>().Property(x => x.Cost).IsOptional();
                        builder.Entity<ServingProductVariant>().Property(x => x.BaseRewards).IsOptional();
                        builder.Entity<ServingProductVariant>().Property(x => x.RewardsExpireDate).IsOptional();

                        // Redemption Catalog
                        builder.Entity<RedemptionCatalog>().Property(x => x.Description).IsOptional();

                        builder.Entity<RedemptionProduct>().Property(x => x.Description).IsOptional();

                        builder.Entity<RedemptionProductVariant>().Property(x => x.Description).IsOptional();
                        builder.Entity<RedemptionProductVariant>().Property(x => x.Price).IsOptional();
                        builder.Entity<RedemptionProductVariant>().Property(x => x.Cost).IsOptional();
                        builder.Entity<RedemptionProductVariant>().Property(x => x.BaseQuantity).IsOptional();
                        builder.Entity<RedemptionProductVariant>().Property(x => x.Code).IsOptional();



                        builder.Entity<ServingRedemptionProductList>().Property(x => x.Description).IsOptional();
                        builder.Entity<ServingRedemptionProductList>().Property(x => x.ImagePath).IsOptional();
                        builder.Entity<ServingRedemptionProductList>().Property(x => x.ImageName).IsOptional();

                        builder.Entity<ServingRedemptionProductVariant>().Property(x => x.Description).IsOptional();
                        builder.Entity<ServingRedemptionProductVariant>().Property(x => x.Price).IsOptional();
                        builder.Entity<ServingRedemptionProductVariant>().Property(x => x.Cost).IsOptional();
                        builder.Entity<ServingRedemptionProductVariant>().Property(x => x.RewardPoints).IsOptional();
                        builder.Entity<ServingRedemptionProductVariant>().Property(x => x.RewardPointsExpireDate).IsOptional();
                        builder.Entity<ServingRedemptionProductVariant>().Property(x => x.Description).IsOptional();


                        builder.Entity<ViewModelDetail>().Property(x => x.Display).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.IsSearchable).IsOptional();

                        //builder.Entity<MemberDetail>().Property(x => x.ManagerID).IsOptional();

                        //builder.Entity<Option>().HasMany(o => o.MDetails)
                        //            .WithRequired(m => m.HowDidYouHear)
                        //            .HasForeignKey(m => m.HowDidYouHearID);

                        //builder.Entity<Option>().HasMany(o => o.MDetails)
                        //            .WithRequired(m => m.Category)
                        //            .HasForeignKey(m => m.CategoryID);

                        //builder.Entity<MemberDetail>().HasRequired(m => m.HowDidYouHear)
                        //            .WithMany(o => o.MDetails)
                        //            .HasForeignKey(m => m.HowDidYouHearID);

                        //builder.Entity<MemberDetail>().HasRequired(m => m.Category)
                        //            .WithMany(o => o.MDetails)
                        //            .HasForeignKey(m => m.CategoryID);

                        //builder.Entity<MemberHouseholdInfo>().HasRequired(m => m.MaritalStatus)
                        //            .WithMany(o => o.MHHInfo)
                        //            .HasForeignKey(m => m.MaritalStatusID);

                        //builder.Entity<MemberHouseholdInfo>().HasRequired(m => m.Income)
                        //           .WithMany(o => o.MHHInfo)
                        //           .HasForeignKey(m => m.IncomeID);

                        builder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
                        builder.Conventions.Remove<OneToOneConstraintIntroductionConvention>();

                        // Client - Root
                        builder.Entity<ProcessLog>().Property(x => x.UserID).IsOptional();
                        builder.Entity<ProcessLog>().Property(x => x.Application).IsOptional();
                        builder.Entity<ProcessLog>().Property(x => x.Log).IsOptional();
                        builder.Entity<ProcessLog>().Property(x => x.Module).IsOptional();
                        builder.Entity<ProcessLog>().Property(x => x.ProcessId).IsOptional();
                        builder.Entity<ProcessLog>().Property(x => x.Comments).IsOptional();

                        builder.Entity<RouteConfiguration>().Property(x => x.ApplicationID).IsOptional();
                        builder.Entity<RouteConfiguration>().Property(x => x.RequestAction).IsOptional();
                        builder.Entity<RouteConfiguration>().Property(x => x.RequestController).IsOptional();
                        builder.Entity<RouteConfiguration>().Property(x => x.ResponseAction).IsOptional();
                        builder.Entity<RouteConfiguration>().Property(x => x.ResponseController).IsOptional();
                        builder.Entity<RouteConfiguration>().Property(x => x.Module).IsOptional();


                        //// Client - Core - Package
                        //builder.Entity<BillingCycle>().Property(x => x.Description).IsOptional();

                        //builder.Entity<Feature>().Property(x => x.Description).IsOptional();

                        //builder.Entity<FeatureModule>().Property(x => x.Description).IsOptional();

                        //builder.Entity<Package>().Property(x => x.Description).IsOptional();


                        // Client -> Setup
                        builder.Entity<Option>().Property(x => x.IsDefault).IsOptional();
                        builder.Entity<Option>().Property(x => x.ParentID).IsOptional();

                        builder.Entity<ViewModel>().Property(x => x.ViewCSS).IsOptional();
                        builder.Entity<ViewModel>().Property(x => x.Entity).IsOptional();
                        builder.Entity<ViewModel>().Property(x => x.MetaDescription).IsOptional();
                        builder.Entity<ViewModel>().Property(x => x.MetaKeyword).IsOptional();
                        builder.Entity<ViewModel>().Property(x => x.MetaTitle).IsOptional();

                        builder.Entity<ViewModelDetail>().Property(x => x.FieldLabelName).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.Name).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.ControlType).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.UIServingClassName).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.CSSClassName).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.LabelCSSClassName).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.Placeholder).IsOptional();

                        builder.Entity<ViewModelDetail>().Property(x => x.DropDownJson).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.Help).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.ValidationMessage).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.SourceEntity).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.AllowSearching).IsOptional();
                        builder.Entity<ViewModelDetail>().Property(x => x.Display).IsOptional();

                        builder.Entity<ViewModelLanguageDetail>().Property(x => x.FieldLabelName).IsOptional();
                        builder.Entity<ViewModelLanguageDetail>().Property(x => x.Help).IsOptional();
                        builder.Entity<ViewModelLanguageDetail>().Property(x => x.ValidationMessage).IsOptional();
                        builder.Entity<ViewModelLanguageDetail>().Property(x => x.Placeholder).IsOptional();
                        builder.Entity<ViewModelLanguageDetail>().Property(x => x.DatabaselName).IsOptional();

                        builder.Entity<ValidationLanguageDetail>().Property(x => x.LanguageCode).IsOptional();


                        builder.Entity<CMSWorkFlow>().Property(x => x.PublishDate).IsOptional();
                        builder.Entity<CMSWorkFlow>().Property(x => x.EntityName).IsOptional();

                        builder.Entity<Permissions>().Property(x => x.MemberID).IsOptional();
                        //builder.Entity<Permissions>().Property(x => x.Title).IsOptional();
                        //builder.Entity<Permissions>().Property(x => x.ControllerName).IsOptional();
                        //builder.Entity<Permissions>().Property(x => x.ActionName).IsOptional();
                        builder.Entity<Ticket>().Property(x => x.ClosedBy).IsOptional();
                        builder.Entity<Ticket>().Property(x => x.DateClosed).IsOptional();
                        builder.Entity<Menu>().Property(x => x.MetaKeyword).IsOptional();
                        builder.Entity<Menu>().Property(x => x.CssClass).IsOptional();


                        #endregion

                        #region Location

                        builder.Entity<Location>().ToTable("Location", schema.ToString());

                        #endregion

                        #region Member
                        //builder.Entity<AddressType>().ToTable("AddressType", schema.ToString());
                        builder.Entity<Member>().ToTable("Member", schema.ToString());
                        builder.Entity<MemberActivity>().ToTable("MemberActivity", schema.ToString());
                        builder.Entity<MemberAddress>().ToTable("MemberAddress", schema.ToString());
                        builder.Entity<MemberChangePassword>().ToTable("MemberChangePassword", schema.ToString());
                        builder.Entity<MemberCommunicationPreference>().ToTable("MemberCommunicationPreference", schema.ToString());
                        builder.Entity<MemberContactInfo>().ToTable("MemberContactInfo", schema.ToString());
                        builder.Entity<MemberCurrentStatus>().ToTable("MemberCurrentStatus", schema.ToString());
                        builder.Entity<MemberCurrentType>().ToTable("MemberCurrentType", schema.ToString());
                        builder.Entity<MemberDetail>().ToTable("MemberDetail", schema.ToString());
                        builder.Entity<MemberForgotPassword>().ToTable("MemberForgotPassword", schema.ToString());
                        builder.Entity<MemberHouseholdInfo>().ToTable("MemberHouseholdInfo", schema.ToString());
                        builder.Entity<MemberPasswordLog>().ToTable("MemberPasswordLog", schema.ToString());
                        builder.Entity<MemberPoint>().ToTable("MemberPoint", schema.ToString());
                        builder.Entity<MemberProfile>().ToTable("MemberProfile", schema.ToString());
                        builder.Entity<MemberProgram>().ToTable("MemberProgram", schema.ToString());
                        builder.Entity<MemberRegion>().ToTable("MemberRegion", schema.ToString());
                        builder.Entity<MemberRegistration>().ToTable("MemberRegistration", schema.ToString());
                        builder.Entity<MemberSalesAdvisor>().ToTable("MemberSalesAdvisor", schema.ToString());
                        builder.Entity<MemberSecurityQuestion>().ToTable("MemberSecurityQuestion", schema.ToString());
                        builder.Entity<MemberStatusCycle>().ToTable("MemberStatusCycle", schema.ToString());

                        builder.Entity<MemberTerritory>().ToTable("MemberTerritory", schema.ToString());
                        builder.Entity<MemberTypeCycle>().ToTable("MemberTypeCycle", schema.ToString());

                        builder.Entity<Region>().ToTable("Region", schema.ToString());
                        builder.Entity<SecurityQuestion>().ToTable("SecurityQuestion", schema.ToString());
                        builder.Entity<Territory>().ToTable("Territory", schema.ToString());
                        builder.Entity<MemberProfileViewData>().ToTable("MemberProfileViewData", schema.ToString());
                        builder.Entity<MemberPortal>().ToTable("MemberPortal", schema.ToString());
                        builder.Entity<MemberType>().ToTable("MemberType", schema.ToString());

                        #endregion

                        #region Purchase Offer / Promotion

                        builder.Entity<PurchaseOffer>().ToTable("PurchaseOffer", schema.ToString());
                        builder.Entity<PurchaseOfferLocation>().ToTable("PurchaseOfferLocation", schema.ToString());
                        builder.Entity<PurchaseOfferMember>().ToTable("PurchaseOfferMember", schema.ToString());
                        builder.Entity<PurchaseOfferProduct>().ToTable("PurchaseOfferProduct", schema.ToString());

                        builder.Entity<PurchasePromotion>().ToTable("PurchasePromotion", schema.ToString());
                        builder.Entity<PurchasePromotionLocation>().ToTable("PurchasePromotionLocation", schema.ToString());
                        builder.Entity<PurchasePromotionMember>().ToTable("PurchasePromotionMember", schema.ToString());
                        builder.Entity<PurchasePromotionProduct>().ToTable("PurchasePromotionProduct", schema.ToString());

                        #endregion

                        #region Redemption Offer / Promotion

                        builder.Entity<RedemptionOffer>().ToTable("RedemptionOffer", schema.ToString());
                        builder.Entity<RedemptionOfferProduct>().ToTable("RedemptionOfferProduct", schema.ToString());

                        #endregion

                        #region Order
                        builder.Entity<ChargeType>().ToTable("ChargeType", schema.ToString());
                        builder.Entity<Order>().ToTable("Order", schema.ToString());
                        builder.Entity<OrderBillingInfo>().ToTable("OrderBillingInfo", schema.ToString());
                        builder.Entity<OrderDetail>().ToTable("OrderDetail", schema.ToString());
                        builder.Entity<OrderDiscount>().ToTable("OrderDiscount", schema.ToString());
                        builder.Entity<OrderPaymentInfo>().ToTable("OrderPaymentInfo", schema.ToString());
                        builder.Entity<OrderPaymentSummary>().ToTable("OrderPaymentSummary", schema.ToString());
                        builder.Entity<OrderShippingCharge>().ToTable("OrderShippingCharge", schema.ToString());
                        builder.Entity<OrderShippingInfo>().ToTable("OrderShippingInfo", schema.ToString());
                        builder.Entity<OrderTax>().ToTable("OrderTax", schema.ToString());
                        builder.Entity<ReferenceOrder>().ToTable("ReferenceOrder", schema.ToString());
                        builder.Entity<ReferenceOrderDetail>().ToTable("ReferenceOrderDetail", schema.ToString());

                        //builder.Entity<ServingOrderDetail>().ToTable("ServingOrderDetail", schema.ToString());
                        //builder.Entity<ServingOrderList>().ToTable("ServingOrderList", schema.ToString());

                        builder.Entity<RedemptionOrder>().ToTable("RedemptionOrder", schema.ToString());
                        builder.Entity<RedemptionOrderBillingInfo>().ToTable("RedemptionOrderBillingInfo", schema.ToString());
                        builder.Entity<RedemptionOrderDetail>().ToTable("RedemptionOrderDetail", schema.ToString());
                        builder.Entity<RedemptionOrderPaymentInfo>().ToTable("RedemptionOrderPaymentInfo", schema.ToString());
                        builder.Entity<RedemptionOrderShippingCharge>().ToTable("RedemptionOrderShippingCharge", schema.ToString());
                        builder.Entity<RedemptionOrderShippingInfo>().ToTable("RedemptionOrderShippingInfo", schema.ToString());
                        builder.Entity<ReferenceRedemptionOrder>().ToTable("ReferenceRedemptionOrder", schema.ToString());
                        builder.Entity<ReferenceRedemptionOrderDetail>().ToTable("ReferenceRedemptionOrderDetail", schema.ToString());

                        #endregion

                        #region Partner
                        builder.Entity<Partner>().ToTable("Partner", schema.ToString());
                        builder.Entity<PartnerCategory>().ToTable("PartnerCategory", schema.ToString());
                        #endregion

                        #region Payment

                        #endregion

                        #region Product
                        //<----- Common classes from Product ----->
                        builder.Entity<Brand>().ToTable("Brand", schema.ToString());
                        builder.Entity<BrandCategory>().ToTable("BrandCategory", schema.ToString());
                        builder.Entity<Category>().ToTable("Category", schema.ToString());
                        builder.Entity<Property>().ToTable("Property", schema.ToString());
                        builder.Entity<PropertyValue>().ToTable("PropertyValue", schema.ToString());

                        //<----- Purchase Product ----->
                        builder.Entity<PurchaseCatalog>().ToTable("PurchaseCatalog", schema.ToString());
                        builder.Entity<PurchaseCatalogProduct>().ToTable("PurchaseCatalogProduct", schema.ToString());
                        builder.Entity<PurchaseProduct>().ToTable("PurchaseProduct", schema.ToString());
                        builder.Entity<PurchaseProductVariant>().ToTable("PurchaseProductVariant", schema.ToString());
                        builder.Entity<PurchaseProductCategory>().ToTable("PurchaseProductCategory", schema.ToString());
                        builder.Entity<PurchaseProductImage>().ToTable("PurchaseProductImage", schema.ToString());
                        builder.Entity<PurchaseProductProperty>().ToTable("PurchaseProductProperty", schema.ToString());

                        builder.Entity<ServingProductByCatalog>().ToTable("ServingProductByCatalog", schema.ToString());
                        builder.Entity<ServingProductByCategory>().ToTable("ServingProductByCategory", schema.ToString());
                        builder.Entity<ServingProductVariant>().ToTable("ServingProductVariant", schema.ToString());
                        builder.Entity<ServingProductImage>().ToTable("ServingProductImage", schema.ToString());
                        builder.Entity<ServingProductList>().ToTable("ServingProductList", schema.ToString());

                        //<----- Redemption Product ----->
                        builder.Entity<RedemptionCatalog>().ToTable("RedemptionCatalog", schema.ToString());
                        builder.Entity<RedemptionCatalogProduct>().ToTable("RedemptionCatalogProduct", schema.ToString());
                        builder.Entity<RedemptionProduct>().ToTable("RedemptionProduct", schema.ToString());
                        builder.Entity<RedemptionProductCategory>().ToTable("RedemptionProductCategory", schema.ToString());
                        builder.Entity<RedemptionProductImage>().ToTable("RedemptionProductImage", schema.ToString());
                        builder.Entity<RedemptionProductOtherInfo>().ToTable("RedemptionProductOtherInfo", schema.ToString());
                        builder.Entity<RedemptionProductProperty>().ToTable("RedemptionProductProperty", schema.ToString());
                        builder.Entity<RedemptionProgramProduct>().ToTable("RedemptionProgramProduct", schema.ToString());
                        builder.Entity<RedemptionCartLog>().ToTable("RedemptionCartLog", schema.ToString());
                        builder.Entity<RedemptionProductVariant>().ToTable("RedemptionProductVariant", schema.ToString());

                        builder.Entity<ServingRedemptionProductByCatalog>().ToTable("ServingRedemptionProductByCatalog", schema.ToString());
                        builder.Entity<ServingRedemptionProductByCategory>().ToTable("ServingRedemptionProductByCategory", schema.ToString());
                        builder.Entity<ServingRedemptionProductImage>().ToTable("ServingRedemptionProductImage", schema.ToString());
                        builder.Entity<ServingRedemptionProductList>().ToTable("ServingRedemptionProductLists", schema.ToString());
                        builder.Entity<ServingRedemptionProductVariant>().ToTable("ServingRedemptionProductVariant", schema.ToString());


                        builder.Entity<Ticket>().ToTable("Ticket", schema.ToString());
                        builder.Entity<TicketComment>().ToTable("TicketComment", schema.ToString());
                        builder.Entity<TicketDocument>().ToTable("TicketDocument", schema.ToString());

                        #endregion

                        #region Client Root
                        builder.Entity<Entities>().ToTable("Entities", schema.ToString());

                        builder.Entity<ViewModelTracking>().ToTable("ViewModelTracking", schema.ToString());
                        builder.Entity<MenulLanguageDetail>().ToTable("MenulLanguageDetail", schema.ToString());

                        builder.Entity<Entities>().ToTable("Entities", schema.ToString());
                        builder.Entity<ProcessLog>().ToTable("ProcessLog", schema.ToString());
                        builder.Entity<RouteConfiguration>().ToTable("RouteConfiguration", schema.ToString());
                        builder.Entity<Menu>().ToTable("Menu", schema.ToString());

                        #endregion

                        #region Security

                        builder.Entity<Permissions>().ToTable("Permissions", schema.ToString());

                        #endregion

                        #region CSS Theme

                        // CSS Theme
                        builder.Entity<CSSLayout>().ToTable("CSSLayout", schema.ToString());
                        builder.Entity<CSSRepositoryThemeVariableValue>().ToTable("CSSRepositoryThemeVariableValue", schema.ToString());
                        builder.Entity<CSSTheme>().ToTable("CSSTheme", schema.ToString());
                        builder.Entity<CSSThemeRepository>().ToTable("CSSThemeRepository", schema.ToString());
                        builder.Entity<CSSVariable>().ToTable("CSSVariable", schema.ToString());
                        builder.Entity<CSSVariableDefaultValue>().ToTable("CSSVariableDefaultValue", schema.ToString());
                        builder.Entity<CSSThemeVariableValue>().ToTable("CSSThemeVariableValue", schema.ToString());
                        builder.Entity<CSSWebsiteTheme>().ToTable("CSSWebsiteTheme", schema.ToString());

                        #endregion

                        #region Report Configuration

                        // CSS Theme
                        builder.Entity<ControlType>().ToTable("ControlType", schema.ToString());
                        builder.Entity<Report>().ToTable("Report", schema.ToString());
                        builder.Entity<ReportGroup>().ToTable("ReportGroup", schema.ToString());
                        builder.Entity<ReportSubGroup>().ToTable("ReportSubGroup", schema.ToString());
                        builder.Entity<ReportTable>().ToTable("ReportTable", schema.ToString());
                        builder.Entity<ReportTableColumn>().ToTable("ReportTableColumn", schema.ToString());

                        #endregion

                        #region Report Configuration

                        builder.Entity<MemberPurchaseSummaryByYearMonth>().ToTable("MemberPurchaseSummaryByYearMonth", schema.ToString());

                        #endregion

                        #region Content Management

                        builder.Entity<ContentDetail>().ToTable("ContentDetail", schema.ToString());
                        builder.Entity<StaticPage>().ToTable("StaticPage", schema.ToString());
                        builder.Entity<FAQ>().ToTable("FAQ", schema.ToString());
                        builder.Entity<FAQAnswer>().ToTable("FAQAnswer", schema.ToString());

                        #endregion


                        //#region Client -> Core -> Package

                        //builder.Entity<Entities>().ToTable("BillingCycle", schema.ToString());
                        //builder.Entity<Entities>().ToTable("ClientPackage", schema.ToString());
                        //builder.Entity<Entities>().ToTable("Feature", schema.ToString());
                        //builder.Entity<Entities>().ToTable("FeatureModule", schema.ToString());
                        //builder.Entity<Entities>().ToTable("Package", schema.ToString());

                        //#endregion

                        #region Setup

                        builder.Entity<MemberProgram>().ToTable("MemberProgram", schema.ToString());

                        builder.Entity<Module>().ToTable("Module", schema.ToString());
                        builder.Entity<Option>().ToTable("Option", schema.ToString());
                        builder.Entity<SubModule>().ToTable("SubModule", schema.ToString());
                            
                        builder.Entity<ViewModel>().ToTable("ViewModel", schema.ToString());
                        builder.Entity<ViewModelDetail>().ToTable("ViewModelDetail", schema.ToString());
                        builder.Entity<ViewModelLanguageDetail>().ToTable("ViewModelLanguageDetail", schema.ToString());
                        builder.Entity<ViewModelValidation>().ToTable("ViewModelValidation", schema.ToString());
                        builder.Entity<ValidationLanguageDetail>().ToTable("ValidationLanguageDetail", schema.ToString());
                        builder.Entity<CMSWorkFlow>().ToTable("CMSWorkFlow", schema.ToString());


                        builder.Entity<ViewModelMessages>().ToTable("ViewModelMessages", schema.ToString());
                        builder.Entity<MessagesLanguageDetail>().ToTable("MessagesLanguageDetail", schema.ToString());

                        #endregion

                        // builder.Entity<ViewModelEntities>().ToTable("ViewModelEntities", schema.ToString());

                        //builder.Entity<ProductMaster>()
                        //.HasRequired()
                        try
                        {
                            var model = builder.Build(connection);
                            return model.Compile();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }

                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                });

            var context = new AppCreateDatabaseContext(connection, compiledModel);
            context.Configuration.LazyLoadingEnabled = true;
            context.Configuration.ProxyCreationEnabled = true;
            return context;
        }

        public static void InitializeTenant(Guid schema, DbConnection connection)
        {
            using (var _ctx = Create(schema, connection))
            {
                if (!_ctx.Database.Exists())
                {
                    _ctx.Database.Create();
                }
                else
                {
                    Database.SetInitializer<AppCreateDatabaseContext>(null);
                    var createScript = ((IObjectContextAdapter)_ctx).ObjectContext.CreateDatabaseScript();
                    _ctx.Database.ExecuteSqlCommand(createScript);
                    // CreateApplication(schema, connection);
                }

                //BuidTenantStartupData(_ctx);
                //BuildDefaultData(_ctx, identifierHOCODE);

                ////On this stage we have schema tabels for tannant so we can directlly call context
                //BuildAdminProfile(_ctx, schema, identifierHOCODE);
                //BuildBusinessManagerProfile(_ctx, schema, identifierHOCODE);
                //BuildSubscriptionManagerProfile(_ctx, schema, identifierHOCODE);
                //BuildSaleRepProfile(_ctx, schema, identifierHOCODE);
                //BuildSocialProfile(_ctx, schema, identifierHOCODE); 
                //AddAdminProfile(UserID, _ctx, schema);
                AddEntities(_ctx, schema);
                AddModules(_ctx, schema);
                AddSubModules(_ctx, schema);
                AddOptions(_ctx, schema);
                AddBrand(_ctx, schema);
                AddMemberTypes(_ctx, schema);
                AddMenu(_ctx, schema);
                AddPermissions(_ctx, schema);
                
                //New
                AddCategory(_ctx, schema);
                //AddEntities(_ctx, schema);
                AddFAQ(_ctx, schema);
                AddFAQAnswer(_ctx, schema);
                //AddMemberType(_ctx, schema);
                //AddMenu(_ctx, schema);
                AddMenulLanguageDetail(_ctx, schema);
                AddMessagesLanguageDetail(_ctx, schema);
                //AddModule(_ctx, schema);
                AddOption(_ctx, schema);
                //AddPermissions(_ctx, schema);
                AddStaticPage(_ctx, schema);
                //AddSubModule(_ctx, schema);
                AddValidationLanguageDetail(_ctx, schema);
                AddViewModel(_ctx, schema);
                AddViewModelDetail(_ctx, schema);
                AddViewModelLanguageDetail(_ctx, schema);
                AddViewModelMessages(_ctx, schema);
                AddViewModelValidation(_ctx, schema);

            }
        }
        private static void AddCategory(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<Category> lstModules = JsonConvert.DeserializeObject<List<Category>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\Category.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.Category.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddFAQ(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<FAQ> lstModules = JsonConvert.DeserializeObject<List<FAQ>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\FAQ.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.FAQ.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddFAQAnswer(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<FAQAnswer> lstModules = JsonConvert.DeserializeObject<List<FAQAnswer>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\FAQAnswer.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.FAQAnswer.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddMemberType(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<MemberType> lstModules = JsonConvert.DeserializeObject<List<MemberType>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\MemberType.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.MemberType.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddMenulLanguageDetail(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<MenulLanguageDetail> lstModules = JsonConvert.DeserializeObject<List<MenulLanguageDetail>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\MenulLanguageDetail.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.MenulLanguageDetail.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddMessagesLanguageDetail(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<MessagesLanguageDetail> lstModules = JsonConvert.DeserializeObject<List<MessagesLanguageDetail>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\MessagesLanguageDetail.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.MessagesLanguageDetail.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddModule(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<Module> lstModules = JsonConvert.DeserializeObject<List<Module>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\Module.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.Module.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddOption(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<Option> lstModules = JsonConvert.DeserializeObject<List<Option>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\Option.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.Option.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddStaticPage(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<StaticPage> lstModules = JsonConvert.DeserializeObject<List<StaticPage>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\StaticPage.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.StaticPage.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddSubModule(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<SubModule> lstModules = JsonConvert.DeserializeObject<List<SubModule>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\SubModule.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.SubModule.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddValidationLanguageDetail(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<ValidationLanguageDetail> lstModules = JsonConvert.DeserializeObject<List<ValidationLanguageDetail>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\ValidationLanguageDetail.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.ValidationLanguageDetail.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddViewModel(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<ViewModel> lstModules = JsonConvert.DeserializeObject<List<ViewModel>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\ViewModel.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.ViewModel.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddViewModelDetail(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<ViewModelDetail> lstModules = JsonConvert.DeserializeObject<List<ViewModelDetail>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\ViewModelDetail.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.ViewModelDetail.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddViewModelLanguageDetail(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<ViewModelLanguageDetail> lstModules = JsonConvert.DeserializeObject<List<ViewModelLanguageDetail>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\ViewModelLanguageDetail.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.ViewModelLanguageDetail.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddViewModelMessages(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<ViewModelMessages> lstModules = JsonConvert.DeserializeObject<List<ViewModelMessages>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\ViewModelMessages.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.ViewModelMessages.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddViewModelValidation(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<ViewModelValidation> lstModules = JsonConvert.DeserializeObject<List<ViewModelValidation>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\ClientDBJson\ViewModelValidation.json"));
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.ViewModelValidation.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddSubModules(AppCreateDatabaseContext _ctx, Guid schema)
        {
            var mid = _ctx.Module.Where(x => x.Name.Trim().ToLower().Equals("member")).FirstOrDefault();
            var pid = _ctx.Module.Where(x => x.Name.Trim().ToLower().Equals("product")).FirstOrDefault();
            var sid = _ctx.Module.Where(x => x.Name.Trim().ToLower().Equals("security")).FirstOrDefault();
            var oid = _ctx.Module.Where(x => x.Name.Trim().ToLower().Equals("order")).FirstOrDefault();

            if (mid != null)
            {
                List<SubModule> lstModules = new List<Client.SubModule> {
                new Client.SubModule { Name = "Gender", ModuleID = mid.ID },
                new Client.SubModule {Name = "Title", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Country", ModuleID = mid.ID },
                new Client.SubModule {Name = "Province", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Member Status", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Profile Type", ModuleID = mid.ID  },
                new Client.SubModule {Name = "How Did You Hear About Us", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Category", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Member Type", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Transaction Type", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Activity Type", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Reward Channel", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Reward Category", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Reward Source", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Reward Reason", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Reward Type", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Transaction Status", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Data Source", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Language", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Income", ModuleID = mid.ID  },
                new Client.SubModule {Name = "MaritalStatus", ModuleID = mid.ID  },
                new Client.SubModule {Name = "ProductType", ModuleID = pid.ID  },
                new Client.SubModule {Name = "ContentType", ModuleID = sid.ID  },
                new Client.SubModule {Name = "Address Type", ModuleID = mid.ID  },
                new Client.SubModule {Name = "Product Status", ModuleID = oid.ID  },
                new Client.SubModule {Name = "Reference Redeem Order Status", ModuleID = oid.ID  },
                new Client.SubModule {Name = "Redemption Cart Status", ModuleID = oid.ID  },
            };


                foreach (var item in lstModules)
                {
                    _ctx.Entry(item).State = EntityState.Added;
                    _ctx.SubModule.Add(item);
                }

                _ctx.SaveChanges();
            }
        }
        private static void AddModules(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<Module> lstModules = new List<Client.Module> {
                new Client.Module {Name = "Member"},
                new Client.Module {Name = "Product"},
                new Client.Module {Name = "Order"},
                new Client.Module {Name = "Security"},
                new Client.Module {Name = "Support"},
                new Client.Module {Name = "Content"}
            };


            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.Module.Add(item);
            }

            _ctx.SaveChanges();
        }
        private static void AddBrand(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<Brand> lstModules = new List<Client.Brand> {
                new Client.Brand {Name = "Brand 1" },
                new Client.Brand {Name = "Brand 2" }
            };
            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.Brand.Add(item);
            }
            _ctx.SaveChanges();
        }
        private static void AddEntities(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<Language> lstLanguages = new List<Client.Language> {
                new Client.Language {Name = "en" },
                new Client.Language {Name = "fr" }
            };

            List<Entities> lstEntities = new List<Client.Entities> {
                // Member
                new Client.Entities { EntityName = "Member", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberAddress", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberCommunicationPreference", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberContactInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberCurrentStatus", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberCurrentType", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberForgotPassword", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberHouseholdInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberPasswordLog", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberPoint", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberProfile", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberProgram", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberRegion", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberRegistration", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberChangePassword", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberSalesAdvisor", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberSecurityQuestion", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberStatusCycle", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberTerritory", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberTypeCycle", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "Region", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "SecurityQuestion", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "Territory", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberProfileViewData", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MemberPortal", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // Location
                new Client.Entities { EntityName = "Location", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // Purchase Offer 
                new Client.Entities { EntityName = "PurchaseOffer", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchaseOfferLocation", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchaseOfferMember", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchaseOfferProduct", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                new Client.Entities { EntityName = "PurchasePromotion", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchasePromotionLocation", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchasePromotionMember", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchasePromotionProduct", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // Redemption Offer
                new Client.Entities { EntityName = "RedemptionOffer", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionOfferProduct", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                
                // Order
                new Client.Entities { EntityName = "ChargeType", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "Order", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "OrderBillingInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "OrderDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "OrderDiscount", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "OrderPaymentInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "OrderPaymentSummary", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "OrderShippingCharge", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "OrderShippingInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "OrderTax", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ReferenceOrder", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ReferenceOrderDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                //new Client.Entities { EntityName = "ServingOrderDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                //new Client.Entities { EntityName = "ServingOrderList", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                new Client.Entities { EntityName = "RedemptionOrder", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionOrderBillingInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionOrderDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionOrderPaymentInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionOrderShippingInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ReferenceRedemptionOrder", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ReferenceRedemptionOrderDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // Add Serving Redemption Order entities here...
                
                // Product: Add Product Entities here.... 
                new Client.Entities { EntityName = "Brand", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "BrandCategory", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "Category", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "Property", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PropertyValue", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionCartLog", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                new Client.Entities { EntityName = "PurchaseCatalog", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchaseCatalogProduct", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchaseProduct", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchaseProductCategory", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchaseProductImage", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "PurchaseProductProperty", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                new Client.Entities { EntityName = "ServingProductByCatalog", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ServingProductByCategory", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ServingProductImage", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ServingProductList", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ServingProductVariant", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                new Client.Entities { EntityName = "RedemptionCatalog", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionCatalogProduct", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionProduct", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionProductCategory", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionProductImage", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionProductOtherInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionProductProperty", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionProductVariant", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionProgramProduct", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                 new Client.Entities { EntityName = "ServingRedemptionProductByCatalog", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                 new Client.Entities { EntityName = "ServingRedemptionProductByCategory", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                 new Client.Entities { EntityName = "ServingRedemptionProductImage", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                 new Client.Entities { EntityName = "ServingRedemptionProductList", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                 new Client.Entities { EntityName = "ServingRedemptionProductVariant", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // Client
                new Client.Entities { EntityName = "Entities", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ProcessLog", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RouteConfiguration", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now }, 

                // Setup
                new Client.Entities { EntityName = "Module", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "Option", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "SubModule", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                new Client.Entities { EntityName = "ViewModel", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ViewModelDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                //new Client.Entities { EntityName = "ViewModelEntities", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ViewModelLanguageDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // Core 
                new Client.Entities { EntityName = "ClientProgram", IsActive = false, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "GlobalServer", IsActive = false, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "GlobalUser", IsActive = false, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "Programs", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ProgramWebsites", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionOrderBillingInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "RedemptionOrderShippingInfo", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "Province", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MenulLanguageDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // CSS Theme
                new Client.Entities { EntityName = "CSSLayout", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "CSSRepositoryThemeVariableValue", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "CSSTheme", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "CSSThemeRepository", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "CSSThemeVariableValue", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "CSSVariable", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "CSSVariableDefaultValue", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "CSSWebsiteTheme", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // Ticket
                new Client.Entities { EntityName = "Ticket", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "TicketComment", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "TicketDocument", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // Report Configuration
                new Client.Entities { EntityName = "ControlType", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "Report", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ReportGroup", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ReportSubGroup", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ReportTable", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ReportTableColumn", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // Reports
                new Client.Entities { EntityName = "MemberPurchaseSummaryByYearMonth", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },

                // Content Management
                new Client.Entities { EntityName = "ContentDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "StaticPage", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "FAQ", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "FAQAnswer", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ValidationLanguageDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "CMSWorkFlow", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ViewModelValidation", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "ValidationLanguageDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },


                new Client.Entities { EntityName = "ViewModelMessges", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now },
                new Client.Entities { EntityName = "MessagesLanguageDetail", IsActive = true, RowID = Guid.NewGuid(), DateCreated = DateTime.Now } 
                
                // SubModule Setup: Insert records to Module, SubModule and Option table here...


                //.............
            };


            foreach (var item in lstEntities)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.Entities.Add(item);
            }

            _ctx.SaveChanges();
        }
        private static void AddOptions(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<Option> lstModules = new List<Client.Option> {
             new Client.Option {Name = "Male",SubModuleID=1, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Female",SubModuleID=1, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Mr.",SubModuleID=2, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Miss",SubModuleID=2, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Ms.",SubModuleID=2, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Mrs.",SubModuleID=2, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Dr.",SubModuleID=2, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Canada",SubModuleID=3, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "USA",SubModuleID=3, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "International",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Alberta",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "British Columbia ",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Manitoba",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "New Brunswick",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Newfoundland and Labrador",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Northwest Territories",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Nova Scotia",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Nunavut",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Ontario",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Prince Edward Island",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Quebec",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Saskatchewan",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Yukon",SubModuleID=4, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Active",SubModuleID=5, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Inactive",SubModuleID=5, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Private",SubModuleID=6, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Public",SubModuleID=6, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Platinum",SubModuleID=8, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Gold",SubModuleID=8, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Silver",SubModuleID=8, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Internet",SubModuleID=7, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "News",SubModuleID=7, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Friend",SubModuleID=7, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Member",SubModuleID=9, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Customer",SubModuleID=9, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Student",SubModuleID=9, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Rewards Credit",SubModuleID=10, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Rewards Debit",SubModuleID=10, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Product Purchase",SubModuleID=11, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Product Returned",SubModuleID=11, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Transfer In",SubModuleID=11, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Transfer Out",SubModuleID=11, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Reward Adjustment",SubModuleID=11, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Online",SubModuleID=12, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "In-Store",SubModuleID=12, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Social",SubModuleID=12, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Website",SubModuleID=13, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Electronic",SubModuleID=14, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Travel",SubModuleID=14, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Fixed",SubModuleID=16, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Percentage",SubModuleID=16, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Pending",SubModuleID=17, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Approved",SubModuleID=17, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Online Feed",SubModuleID=18, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Manual Submit",SubModuleID=18, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "File Submit",SubModuleID=18, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Reward Reason 1",SubModuleID=15, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Reward Reason 2",SubModuleID=15, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Income 1",SubModuleID=20, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Income 2",SubModuleID=20, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "English",SubModuleID=19, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "French",SubModuleID=19, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "MaritalStatus1",SubModuleID=21, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "MaritalStatus 2",SubModuleID=21, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Address Type1",SubModuleID=24, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Address Type2",SubModuleID=24, Description = "", IsActive= true, IsDefault=false, ParentID = 0},

             new Client.Option {Name = "Consumer Goods",SubModuleID=22, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Services",SubModuleID=22, Description = "", IsActive= true, IsDefault=false, ParentID = 0},

             new Client.Option {Name = "Page",SubModuleID=23, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Tab",SubModuleID=23, Description = "", IsActive= true, IsDefault=false, ParentID = 0},

             new Client.Option {Name = "Active",SubModuleID=25, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Inactive",SubModuleID=25, Description = "", IsActive= true, IsDefault=false, ParentID = 0},

             new Client.Option {Name = "Incomplete",SubModuleID=26, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Complete",SubModuleID=26, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Ordered",SubModuleID=26, Description = "", IsActive= true, IsDefault=false, ParentID = 0},

             new Client.Option {Name = "Open",SubModuleID=27, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
             new Client.Option {Name = "Close",SubModuleID=27, Description = "", IsActive= true, IsDefault=false, ParentID = 0},
         };




            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.Option.Add(item);
            }

            _ctx.SaveChanges();
        }
        private static void AddMemberTypes(AppCreateDatabaseContext _ctx, Guid schema)
        {
            List<MemberType> lstModules = new List<Client.MemberType> {
                new Client.MemberType { Name = "Client Admin"},
                new Client.MemberType {Name = "Client Manager" },
                new Client.MemberType {Name = "Client Call Center Manager" },
                new Client.MemberType {Name = "Client Call Center Rep" },
                new Client.MemberType {Name = "Customer" },
                new Client.MemberType {Name = "Store Admin" },
                new Client.MemberType {Name = "Store Manager" } ,
                new Client.MemberType {Name = "Student" }
            };

            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.MemberType.Add(item);
            }

            _ctx.SaveChanges();
        }
        private static void AddMenu(AppCreateDatabaseContext _ctx, Guid schema)
        {

            List<Menu> lstModules = new List<Client.Menu> {
            new Client.Menu { ContentTypeID = 70,   Title = "Customer",ControllerName = "Member",ActionName = "Index", ParentID = 0, ModuleID = 1   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Management", ControllerName = "Management", ActionName = "Index", ParentID =0, ModuleID = 1   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Product", ControllerName = "Product", ActionName = "Index", ParentID = 0, ModuleID = 2   , IsActive = false},
            new Client.Menu { ContentTypeID = 70, Title = "Order", ControllerName = "Order", ActionName = "Index", ParentID = 0, ModuleID = 3   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Customers", ControllerName = "Member", ActionName = "Index", ParentID = 1, ModuleID = 1   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Contact Info", ControllerName = "Member", ActionName = "ContactInfo", ParentID = 1, ModuleID = 1   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Address", ControllerName = "Member", ActionName = "MemberAddress", ParentID = 1, ModuleID = 1   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Communication Preference", ControllerName = "Member", ActionName = "MemberCommunication", ParentID = 1, ModuleID = 1   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Household Information", ControllerName = "Member", ActionName = "MemberHouseholdInfo", ParentID = 1, ModuleID = 1   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Status Cycle", ControllerName = "Member", ActionName = "MemberStatusLifeCycle", ParentID = 1, ModuleID = 1   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Redemption Products", ControllerName = "Product", ActionName = "RedemptionProductList", ParentID = 3, ModuleID = 1   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Communication Preference", ControllerName = "Product", ActionName = "MemberCommunication", ParentID = 3, ModuleID = 1   , IsActive = false},
            new Client.Menu { ContentTypeID = 70, Title = "Household Information", ControllerName = "Product", ActionName = "MemberHouseholdInfo", ParentID = 3, ModuleID = 1   , IsActive = false},
            new Client.Menu { ContentTypeID = 70, Title = "Status Cycle", ControllerName = "Product", ActionName = "MemberStatusLifeCycle", ParentID = 3, ModuleID = 1   , IsActive = false},
            new Client.Menu { ContentTypeID = 70, Title = "Customer", ControllerName = "Customer", ActionName = "Index", ParentID = 0, ModuleID = 1   , IsActive = true},
            new Client.Menu { ContentTypeID = 70, Title = "Address", ControllerName = "Customer", ActionName = "MemberAddress", ParentID = 15, ModuleID = 1   , IsActive = true},
            };

            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.Menu.Add(item);
            }


            _ctx.SaveChanges();
        }
        private static void AddPermissions(AppCreateDatabaseContext _ctx, Guid schema)
        {


            List<Permissions> lstModules = new List<Client.Permissions> {
                new Permissions { MemberType = 1,MemberID = 0,MenuID = 1,CanRead = true,CanAdd = true,CanEdit = true,CanApprove = true,IsActive = true},
                new Permissions {MemberType = 1, MemberID = 0, MenuID = 2, CanRead = true, CanAdd = true, CanEdit = true, CanApprove = true, IsActive = true    },
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   3   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true},
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   4   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true},
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   5   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   6   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   7   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   8   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   9   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   10  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   11  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   12  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   13  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    1   , MemberID= 0   , MenuID=   14  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   1   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   2   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   3   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   4   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   5   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   6   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   7   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   8   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   9   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   10  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    3   , MemberID= 0   , MenuID=   11  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   15  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   2   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   3   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   4   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   5   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   6   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   16  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= true    },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   8   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   9   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   10  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    5   , MemberID= 0   , MenuID=   11  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    3   , MemberID= 4   , MenuID=   2   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    3   , MemberID= 4   , MenuID=   4   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    3   , MemberID= 4   , MenuID=   5   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    3   , MemberID= 4   , MenuID=   9   , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },
                new Permissions {MemberType=    3   , MemberID= 4   , MenuID=   10  , CanRead=  true    , CanAdd=   true    , CanEdit=  true    ,CanApprove=    true    , IsActive= false   },

            };

            foreach (var item in lstModules)
            {
                _ctx.Entry(item).State = EntityState.Added;
                _ctx.Permissions.Add(item);
            }


            _ctx.SaveChanges();
        }

    }
}

