namespace FGC.CoreData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedFeatureAndClientPackageDetailReal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Feature", "DefaultValue", c => c.String());
            AlterColumn("dbo.Feature", "ValueDataType", c => c.String());
            AlterColumn("dbo.Feature", "Name", c => c.String(maxLength: 100));
            AlterColumn("dbo.ClientPackageDetail", "FeatureValue", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ClientPackageDetail", "FeatureValue", c => c.String(nullable: false));
            AlterColumn("dbo.Feature", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Feature", "ValueDataType", c => c.String(nullable: false));
            AlterColumn("dbo.Feature", "DefaultValue", c => c.String(nullable: false));
        }
    }
}
