// <auto-generated />
namespace FGC.CoreData.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ModifiedFeatureAndClientPackageDetail2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ModifiedFeatureAndClientPackageDetail2));
        
        string IMigrationMetadata.Id
        {
            get { return "201708071106001_ModifiedFeatureAndClientPackageDetail-2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
