﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class MemberType : BaseEntity
    {
        public MemberType()
        {

        }
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

    }
}
