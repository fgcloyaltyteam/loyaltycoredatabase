﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class AddressType : BaseEntity
    {
        public int ID { get; set; }
        [Required]
        [MaxLength(50)]
        public string TypeName { get; set; }
    }
}
