﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class MemberCommunicationPreference : BaseEntity
    {
        public MemberCommunicationPreference()
        {
            //Member = new Member();
            IsShowSurvey = false;
            CommunicateViaEmail = false;
            CommunicateViaSms = false;
            ReceiveNotification = false;
            ShowYourInformation = false;
        }

        public int ID { get; set; }

        //[Required, ForeignKey("Member")]
        public int MemberID { get; set; }
        public bool IsShowSurvey { get; set; }
        public bool CommunicateViaEmail { get; set; }
        public bool CommunicateViaSms { get; set; }
        public bool ReceiveNotification { get; set; }
        public bool ShowYourInformation { get; set; }


        //public virtual Member Member { get; set; }
    }
}
