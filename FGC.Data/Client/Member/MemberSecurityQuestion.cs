﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class MemberSecurityQuestion : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public int MemberID { get; set; }

        [Required]
        public int QuestionID { get; set; }

        [Required, MaxLength(100)]
        public string Answer { get; set; }
    }
}
