﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class MemberStatusCycle : BaseEntity
    {
        public MemberStatusCycle()
        {
            
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int MemberID { get; set; }

        [Required]
        public int MemberStatusID { get; set; }

        public string Notes { get; set; }

    }
}
