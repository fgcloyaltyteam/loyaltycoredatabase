﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class CustomerStatusMaster : BaseEntity
    {
        public int Id { get; set; }
        [Required]
        public string StatusName { get; set; }
    }
}
