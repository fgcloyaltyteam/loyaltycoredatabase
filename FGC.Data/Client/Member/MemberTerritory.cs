﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class MemberTerritory: BaseEntity
    {
        public MemberTerritory()
        {

        }

        public int ID { get; set; }

        [Required]
        public int MemberID { get; set; }

        [Required]
        public int TerritoryID { get; set; }
    }
}
