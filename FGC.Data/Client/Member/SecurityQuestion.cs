﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class SecurityQuestion : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        [Required, MaxLength(255)]
        public string Question { get; set; }
    }
}
