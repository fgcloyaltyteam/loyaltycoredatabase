﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;


namespace FGC.Data.Client
{
    public class MemberAddress : BaseEntity
    { 
        public MemberAddress()
        {
            //AddressType = new AddressType();
            //Member = new Member();
            //Option = new Option();
        }
        public int ID { get; set; }

        [Required]//, ForeignKey("Member"), Column(Order = 1)]
        public int MemberID { get; set; }

        [Required]
        public int AddressTypeID { get; set; }

        [Required, MaxLength(200)]
        public string AddressLine1 { get; set; }

        [MaxLength(200)]
        public string AddressLine2 { get; set; }

        [MaxLength(200)]
        public string AddressLine3 { get; set; }

        [Required, MaxLength(100)]
        public string City { get; set; }

        [Required]
        public int ProvinceID { get; set; }

        [Required]
        public int CountryID { get; set; }

        [Required, MaxLength(20)]
        public string PostalCode { get; set; }

        public bool IsDefault { get; set; }


        //public virtual Member Member { get; set; } 
        //public virtual Option Option { get; set; }
    }
}
