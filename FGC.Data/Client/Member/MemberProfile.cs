﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class MemberProfile : BaseEntity
    {
        public MemberProfile()
        { 
            //Member = new Member();
        }
        public int ID { get; set; }

        //[Required, ForeignKey("Member")]
        public int MemberID { get; set; }

        [MaxLength(50)]
        public string partnerMemberID { get; set; }

        [MaxLength(100)]
        public string AccessToken { get; set; }

        [Required, MaxLength(50)]
        public string FirstName { get; set; }

        [Required, MaxLength(50)]
        public string LastName { get; set; }

        [MaxLength(50)]
        public string MiddleInitial { get; set; }
        public int TitleID { get; set; }
        public DateTime DOB { get; set; }
        public int ProfileTypeID { get; set; }
        public int SegmentTypeID { get; set; }
        public int LanguageID { get; set; }
        public int GenderID { get; set; }
         
       // public virtual Member Member { get; set; }
    }
}
