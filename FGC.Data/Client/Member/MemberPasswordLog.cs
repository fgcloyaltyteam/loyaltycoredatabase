﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class MemberPasswordLog : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public int MemberID { get; set; }

        [Required, MaxLength(100)]
        public string PasswordHash { get; set; }

        [Required, MaxLength(100)]
        public string PasswordSalt { get; set; }

        /// <summary>
        /// Could be either Change Password OR Forgot Password OR Reset by Admin
        /// </summary>
        [Required]
        public string Source { get; set; }
    }
}
