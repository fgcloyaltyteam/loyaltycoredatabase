﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class MemberTypeCycle : BaseEntity
    {
        public MemberTypeCycle()
        {
            
        }

        [Key]
        public int ID { get; set; }
       
        [Required]
        public int MemberID { get; set; }

        [Required]
        public int MemberTypeID { get; set; }

    }
}
