﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations;

namespace FGC.Data.Client 
{
    public class MemberPoint : BaseEntity
    {
        public MemberPoint()
        {
           
        }
        public int ID { get; set; }

        [Required]
        public int MemberID { get; set; }

        public decimal Earned { get; set; } 
        public decimal Redeemed { get; set; }
        public decimal Returned { get; set; }
        public decimal Balance { get; set; }
        public decimal TransferredOut { get; set; }
        public decimal TransferredIn { get; set; }

    }
}
