﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class MemberForgotPassword : BaseEntity
    {
        public MemberForgotPassword()
        {
            IsUsed = false;
        }

        [Key]
        public int ID { get; set; }

        public int MemberID { get; set; }

        [Required, MaxLength(200)]
        public string LoginName { get; set; }

        [Required, MaxLength(100)]
        public string PasswordToken { get; set; }

        public string Notes { get; set; }

        [Required]
        public bool IsUsed { get; set; }
    }
}
