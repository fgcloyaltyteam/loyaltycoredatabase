﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;
using System.Collections.Generic;

namespace FGC.Data.Client
{
    public class MemberProgram : BaseEntity
    {
        public MemberProgram()
        {
            
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int ProgramID { get; set; }

        [Required]
        public int MemberID { get; set; } 


    }
}
