﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class MemberChangePassword : BaseEntity
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public int MemberID { get; set; }

        [Required, MaxLength(100)]
        public string NewPassword { get; set; }
    }
}
