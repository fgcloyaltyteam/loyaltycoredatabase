﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class MemberActivity : BaseEntity
    {
        public MemberActivity()
        {
           // Member = new Member();
        }

        [Key]
        public int ID { get; set; }

        /// <summary>
        /// A Transaction ID, Prepared/Calculated by system based on Activity Type
        /// </summary>
        [Required, MaxLength(20)]
        public string TransactionID { get; set; }

        /// <summary>
        /// Data received from thired party may have its own Transaction ID
        /// </summary>
        [MaxLength(20)]
        public string RefTransactionID { get; set; }

        [Required]
        public DateTime TransactionDate { get; set; }

        [Required]
        public DateTime ReceiveDate { get; set; }

        [Required]
        public DateTime PostDate { get; set; }

        public DateTime ShipDate { get; set; }

        /// <summary>
        /// From Member Table
        /// </summary>
        [Required]
        public int MemberID { get; set; }

        /// <summary>
        /// Could be a Transfer in/out from other member, From Member Table
        /// </summary>
        public int RefMemberID { get; set; }

        public decimal PurchaseAmount { get; set; }

        [Required]
        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal Cost { get; set; }

        [MaxLength(500)]
        public string TransactionComment { get; set; }

        /// <summary>
        /// For purchase activity, there may be a Wholesaler, From Member Table based on Member Type ID 
        /// </summary>
        public int WholesalerID { get; set; }

        /// <summary>
        /// There may be a Merchant,  From Member table based on Member Type ID
        /// </summary>
        public int MerchantID { get; set; }

        [Required]
        public decimal MemberRewards { get; set; }

        /// <summary>
        /// Channel, apprpriate ID from Option Table
        /// </summary>
        public int ChannelID { get; set; }

        public int LocationID { get; set; }

        public int SalesRepID { get; set; }

        /// <summary>
        /// Category, apprpriate ID from Option Table
        /// </summary>
        public int CategoryID { get; set; }
        /// <summary>
        /// Reward Source, apprpriate ID from Option Table
        /// </summary>
        public int SourceID { get; set; }
        /// <summary>
        /// Reward Reason, apprpriate ID from Option Table
        /// </summary>
        public int ReasonID { get; set; }

        /// <summary>
        /// Transaction Type, Debit / Credit, apprpriate ID from Option 
        /// </summary>
        [Required]
        public int TransactionTypeID { get; set; }

        /// <summary>
        /// Activity, Purchase, Redeemed, Transfer, Returned etc., apprpriate ID from Option 
        /// </summary>
        [Required]
        public int ActivityTypeID { get; set; }

        /// <summary>
        /// Reward Type, Fixed, Percentage etc., apprpriate ID from Option 
        /// </summary>
        [Required]
        public int RewardTypeID { get; set; }

        /// <summary>
        /// Status, Approved, Hold, Cancelled etc., apprpriate ID from Option 
        /// </summary>
        [Required]
        public int StatusID { get; set; }

        /// <summary>
        /// Offer for which Rewards has been earned, apprpriate ID from Offer table
        /// </summary>
        public int OfferID { get; set; }

        /// <summary>
        /// Need to discuss
        /// </summary>
        public int TokenID { get; set; }

        /// <summary>
        /// Redemption Order ID
        /// </summary>
        public int OrderID { get; set; }

        public int ProductID { get; set; }

        public string ProductSerialNumber { get; set; }

        /// <summary>
        /// Partner Order ID, From Sales Data of partner 
        /// </summary>
        public string PartnerOrderID { get; set; }

        [MaxLength(500)]
        public string UserComment { get; set; }

        [MaxLength(100)]
        public string DataSource { get; set; }

        /// <summary>
        /// This can be removed as we already have a Merchant ID field
        /// </summary>
        public int PartnerMerchantID { get; set; }

        /// <summary>
        /// Brand Company
        /// </summary>
        public int BrandMerchantID { get; set; }

        /// <summary>
        /// Billing Terminal ID for Sales data
        /// </summary>
        public int TerminalID { get; set; }

        /// <summary>
        /// Data Load in Batch
        /// </summary>
        public int FileID { get; set; }

    }
}
