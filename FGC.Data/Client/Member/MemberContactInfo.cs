﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class MemberContactInfo : BaseEntity
    {
        public MemberContactInfo()
        { 
            //Member = new Member();
        }

        public int ID { get; set; }

        //[ForeignKey("Member")]
        public int MemberID { get; set; }

        [MaxLength(100)]
        public string CompanyName { get; set; }

        [Required, EmailAddress, MaxLength(100)]
        public string EmailAddress { get; set; }

        [EmailAddress, MaxLength(100)]
        public string AlternateEmailAddress { get; set; }

        [Required, MaxLength(20)]
        public string Phone { get; set; }

        [MaxLength(20)]
        public string AlternatePhone { get; set; }

        [MaxLength(20)]
        public string Mobile { get; set; }

        [MaxLength(20)]
        public string Fax { get; set; }


       // public virtual Member Member { get; set; }
    }
}
