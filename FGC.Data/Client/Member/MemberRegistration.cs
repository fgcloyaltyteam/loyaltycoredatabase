﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations;

namespace FGC.Data.Client
{
    public class MemberRegistration : BaseEntity
    {
        public MemberRegistration()
        {
           
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int MemberID { get; set; }
        public DateTime JoiningDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public DateTime ApprovalDate { get; set; }
        public DateTime DeactivationDate { get; set; }
        public string IpAddress { get; set; }
        public DateTime RenewedDate { get; set; }
        public DateTime ActivationDate { get; set; }

    }
}
