﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class Member : BaseEntity
    {
        public Member() 
        {
           // this.AddressList = new List<MemberAddress>();
          // this.MemberStatusMaster = new MemberStatusMaster();
           
        }
 
        public int ID { get; set; }

        [MaxLength(100)]
        public string ClientMemberID { get; set; }

        [Required, MaxLength(100)]
        public string LoginName { get; set; }

        [Required, MaxLength(20)]
        public string Password { get; set; }

        [MaxLength(50)]
        public string PasswordSalt { get; set; }

        [MaxLength(50)]
        public string PasswordHashCode { get; set; }

        //[Required]
        //[ForeignKey("MemberStatusMaster")]
        //public int MemberCurrentStatusID { get; set; } 

        //public virtual MemberStatusMaster MemberStatusMaster { get; set; } 

       
    }
}
