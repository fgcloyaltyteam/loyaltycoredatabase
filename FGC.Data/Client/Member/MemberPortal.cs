﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;
using System.Collections.Generic;

namespace FGC.Data.Client
{
    public class MemberPortal : BaseEntity
    {
        public MemberPortal()
        { 
            
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int PortalID { get; set; }
  
        [Required]
        public int MemberTypeID { get; set; }
     
        public string TargetUrl { get; set; }

        [Required]
        public int MemberID { get; set; }
    }
}
