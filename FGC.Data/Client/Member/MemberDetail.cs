﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class MemberDetail : BaseEntity
    {
        public MemberDetail()
        { 
           // Member = new Member();
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int MemberID { get; set; }

        [MaxLength(100)]
        public string ImageFileName { get; set; }
        public int CategoryID { get; set; }
        public string MemberNotes { get; set; }
        public int HowDidYouHearID { get; set; }

    }
}
 