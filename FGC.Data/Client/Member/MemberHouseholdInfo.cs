﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class MemberHouseholdInfo : BaseEntity
    {
        public MemberHouseholdInfo()
        {
            
        }

        public int ID { get; set; }

        [Required]
        public int MemberID { get; set; }

        public int MaritalStatusID { get; set; }
        public int NoOfFamilyMembers { get; set; }
        public int NoOfMaleMembers { get; set; }
        public int NoOfFemaleMembers { get; set; }
        public int NoOfKids { get; set; }
        public int IncomeID{ get; set; }
   }
}
