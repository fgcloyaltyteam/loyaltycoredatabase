﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class PurchasePromotion : BaseEntity
    {
        public PurchasePromotion()
        {

        }

        [Key]
        public int ID { get; set; }

        /// <summary>
        /// OfferID from Purchase Offer
        /// </summary>
        [Required]
        public int OfferID { get; set; }

        [Required, MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Promotion Value in Scoop 2x, 3x,... etc
        /// </summary>
        [Required]
        public int promotionValue { get; set; }

        public DateTime DateApproved { get; set; }

        public int ApprovedBy { get; set; }

        [MaxLength(500)]
        public string ModifyReason { get; set; }

        /// <summary>
        /// Appropriate OptionID from Options
        /// </summary>
        [Required]
        public bool StatusID { get; set; }
    }
}
