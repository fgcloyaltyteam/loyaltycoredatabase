﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class PurchasePromotionProduct: BaseEntity
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public int PromotionID { get; set; }

        [Required]
        public int ProductID { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

    }
}
