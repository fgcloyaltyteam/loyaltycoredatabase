﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class PurchasePromotionLocation : BaseEntity
    {
        public PurchasePromotionLocation()
        {
            ProvinceID = 0;
            LocationID = 0;
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int PromotionID { get; set; }

        [Required]
        public int ProvinceID { get; set; }

        [Required]
        public int LocationID { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public DateTime DateApproved { get; set; }

        public int ApprovedBy { get; set; }
    }
}
