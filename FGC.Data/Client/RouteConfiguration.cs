﻿ 
using FGC.Data.Base;


namespace FGC.Data.Client
{
    public class RouteConfiguration : BaseEntity
    {
        public RouteConfiguration()
        {
             
        }

        public int ID { get; set; }
        public string ApplicationID { get; set; }
        public string RequestAction  { get; set; }
        public string RequestController { get; set; }
        public string ResponseAction { get; set; }
        public string ResponseController { get; set; }
        public string Module { get; set; } 

         
    }
}