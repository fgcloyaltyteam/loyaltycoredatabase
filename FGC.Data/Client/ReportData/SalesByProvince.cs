﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client.ReportData
{
    public class SalesByProvince
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Province { get; set; }

        [Required]
        public decimal Sales { get; set; }
    }
}
