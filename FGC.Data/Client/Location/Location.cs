﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class Location : BaseEntity
    {

        public Location()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required, MaxLength(10)]
        public string LocationCode { get; set; }

        [Required, MaxLength(100)]
        public string LocationName { get; set; }

        [Required]
        public int ChannelID { get; set; }

        [MaxLength(100)]
        public string AddressLine1 { get; set; }

        [MaxLength(100)]
        public string AddressLine2 { get; set; }

        [MaxLength(100)]
        public string AddressLine3 { get; set; }

        [MaxLength(100)]
        public string City { get; set; }

        [MaxLength(20)]
        public string PostalCode { get; set; }

        public int ProvinceID { get; set; }

        public int CountryID { get; set; }

    }
}
