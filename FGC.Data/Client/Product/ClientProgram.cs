﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;
using System.Collections.Generic;

namespace FGC.Data.Client
{


    public class ClientProgram : BaseEntity
    {
        public ClientProgram()
        {
             
        }
        public int ID { get; set; }

        [Required]
        public int ClientID { get; set; }
        public int ProgramID { get; set; }
        public int WebsiteID { get; set; } 
    }
}

