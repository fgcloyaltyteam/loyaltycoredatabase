﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    
    /// <summary>
    /// 
    /// </summary>
    public class RedemptionCartLog : BaseEntity
    {
        public int ID { get; set; }

        [Required, MaxLength(100)]
        public string SessionID { get; set; } 
        public string CartData { get; set; } 
        public int MemberID { get; set; }
        public int Status { get; set; }
    }
}
 
