﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class Category : BaseEntity
    {
        public int ID { get; set; }
        public int ParentID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }
        public string Description { get; set; } 
    }
}

/*


BrandID
BrandName
*/
