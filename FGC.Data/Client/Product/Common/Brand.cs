﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    
    /// <summary>
    /// 
    /// </summary>
    public class Brand : BaseEntity
    {
        public int ID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required]
        public int PartnerID { get; set; }
    }
}
 
