﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class Property : BaseEntity
    {
        public int ID { get; set; }

        // This field will hold a code and the actual language specific text will be stored in Content Detail table
        [Required, MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public int StatusID { get; set; }
    }
}
