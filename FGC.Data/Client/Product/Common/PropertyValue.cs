﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class PropertyValue : BaseEntity
    {
        public int ID { get; set; }

        public int PropertyID { get; set; }

        [Required, MaxLength(100)]
        public string Value { get; set; }

        [Required]
        public int StatusID { get; set; }

    }
}
