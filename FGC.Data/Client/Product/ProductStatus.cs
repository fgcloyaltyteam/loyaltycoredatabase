﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    
    /// <summary>
    /// 
    /// </summary>
    public class ProductStatus : BaseEntity
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; } 
    }
}

 
