﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ServingRedemptionProductList
    {
        public ServingRedemptionProductList()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int ProductID { get; set; }

        [Required]
        public string Code { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public double BaseQuantity { get; set; }

        [Required]
        public double BasePoints { get; set; }

        [Required]
        public int BrandID { get; set; }

        [Required]
        public string BrandName { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// Product Current Status: Appropriate value from Options table
        /// </summary>
        [Required]
        public int StatusID { get; set; }

        [Required]
        public string StatusName { get; set; }
        public string ImagePath { get; set; }
        public string ImageName { get; set; }

    }
}
