﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class RedemptionProgramProduct : BaseEntity
    {
        public RedemptionProgramProduct()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int ProgramID { get; set; }

        [Required]
        public int ProductID { get; set; }
    }
}
