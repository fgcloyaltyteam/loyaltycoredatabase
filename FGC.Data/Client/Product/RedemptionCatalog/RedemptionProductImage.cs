﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    /// <summary>
    /// 
    /// </summary>
    public class RedemptionProductImage : BaseEntity
    {
        public RedemptionProductImage()
        {
            
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int ProductID { get; set; }

        /// <summary>
        /// Image Type: Appropriate value from Option table. Example: Icon, Thumbnail, Main etc
        /// </summary>
        [Required]
        public int ImageTypeID { get; set; }

        [Required, MaxLength(100)]
        public string ImageName { get; set; }

        [Required, MaxLength(100)]
        public string ImagePath { get; set; }

    }
}
 
