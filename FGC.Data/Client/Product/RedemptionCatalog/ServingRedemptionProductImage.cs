﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ServingRedemptionProductImage
    {
        public ServingRedemptionProductImage()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int VariantID { get; set; }

        /// <summary>
        /// Image type: Icon, THumbnail, Detail,  Or by Size
        /// </summary>
        [Required]
        public int TypeID { get; set; }

        [Required, MaxLength(100)]
        public string TypeName { get; set; }

        /// <summary>
        /// Image file name
        /// </summary>
        [Required, MaxLength(255)]
        public string Name { get; set; }
    }
}
