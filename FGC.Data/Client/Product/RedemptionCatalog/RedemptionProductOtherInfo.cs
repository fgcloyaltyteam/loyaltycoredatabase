﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class RedemptionProductOtherInfo : BaseEntity
    {
        public RedemptionProductOtherInfo()
        {
           
        }

        [Key]
        public int ID { get; set; }
        
        [Required]
        public int ProductID { get; set; }

        [Required, MaxLength(100)]
        public string InfoName { get; set; }

        [Required]
        public string InfoValue { get; set; }

    }
}
