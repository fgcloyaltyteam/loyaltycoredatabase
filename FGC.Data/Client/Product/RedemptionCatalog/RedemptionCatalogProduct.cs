﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class RedemptionCatalogProduct : BaseEntity
    {
        public RedemptionCatalogProduct()
        {

        }

        public int ID { get; set; }

        [Required]
        public int CatalogID { get; set; }

        [Required]
        public int ProductID { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

    }
}
 