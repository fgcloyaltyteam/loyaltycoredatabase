﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.Data.Client
{
    public class RedemptionProductProperty : BaseEntity
    {
        public RedemptionProductProperty()
        {
           
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int ProductID { get; set; }

        [Required]
        public int PropertyID { get; set; }

        [Required]
        public int PropertyValueID { get; set; }
         
    }
}
 