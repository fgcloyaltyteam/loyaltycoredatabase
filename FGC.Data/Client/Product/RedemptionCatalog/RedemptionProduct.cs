﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class RedemptionProduct : BaseEntity
    {
        public RedemptionProduct() 
        {

        }

        public int ID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public double BaseQuantity { get; set; }

        [Required]
        public double BasePoints { get; set; }
        /// <summary>
        /// Product Current Status: Appropriate value from Options table
        /// </summary>
        [Required]
        public int StatusID { get; set; }

        [Required]
        public int BrandID { get; set; }

        /// <summary>
        /// Product Type: Appropriate value from Options table
        /// </summary>
        [Required]
        public int TypeID { get; set; }

    }
}

/*
RewardProduct
EligibleProductID
productWeight
ProductStatusID
ProductImage
ProductException
ProductID
Quantity

*/
