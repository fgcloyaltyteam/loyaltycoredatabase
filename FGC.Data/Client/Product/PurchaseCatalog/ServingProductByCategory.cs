﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ServingProductByCategory
    {
        public ServingProductByCategory()
        {

        }
        public int ID { get; set; }

        [Required]
        public int VariantID { get; set; }

        [Required]
        public int CategoryID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }


    }
}
