﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ServingProductList
    {
        public ServingProductList()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int ProductID { get; set; }

        [Required, MaxLength(50)]
        public string Code { get; set; }

        [Required, MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(50)]
        public string SKUCode { get; set; }

        [MaxLength(50)]
        public string UPCCode { get; set; }

        [MaxLength(50)]
        public string GTIN { get; set; }

        public decimal BaseRewards { get; set; }

        [Required]
        public double BaseQuantity { get; set; }

        /// <summary>
        /// Product Current Status: Appropriate value from Options table
        /// </summary>
        [Required]
        public int StatusID { get; set; }

        [Required, MaxLength(100)]
        public string StatusName { get; set; }

        [Required]
        public int BrandID { get; set; }

        [Required, MaxLength(100)]
        public string BrandName { get; set; }

        /// <summary>
        /// Product Type: Appropriate value from Options table
        /// </summary>
        [Required]
        public int TypeID { get; set; }

        [Required, MaxLength(100)]
        public string TypeName { get; set; }

       // public decimal BaseRewards { get; set; }

        public int ImageID { get; set; }

        public string ImageName { get; set; }
    }
}
