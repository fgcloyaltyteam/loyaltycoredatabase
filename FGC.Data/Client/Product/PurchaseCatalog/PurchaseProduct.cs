﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class PurchaseProduct : BaseEntity
    {
        public PurchaseProduct()
        {
            
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public string Code { get; set; }

        [Required, MaxLength(255)]
        public string Name { get; set; }

        public string Description { get; set; }

        [MaxLength(50)]
        public string SKUCode { get; set; }

        [MaxLength(50)]
        public string UPCCode { get; set; }

        [MaxLength(50)]
        public string GTIN { get; set; }

        public decimal BaseRewards { get; set; }

        [Required]
        public decimal BaseQuantity { get; set; }

        public decimal Price { get; set; }

        public decimal Cost { get; set; }

        /// <summary>
        /// Product Current Status: Appropriate value from Options table
        /// </summary>
        [Required]
        public int StatusID { get; set; }

        [Required]
        public int BrandID { get; set; }

        /// <summary>
        /// Product Type: Appropriate value from Options table
        /// </summary>
        [Required]
        public int TypeID { get; set; }
    }
}
