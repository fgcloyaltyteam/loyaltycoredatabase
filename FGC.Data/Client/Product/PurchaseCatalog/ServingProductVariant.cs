﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ServingProductVariant
    {
        public ServingProductVariant()
        {
            IsDefault = false;
            DateModified = DateTime.Now;
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int VariantID { get; set; }

        [Required]
        public int ProductID { get; set; }

        [Required, MaxLength(50)]
        public string Code { get; set; }

        [Required, MaxLength(255)]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public double BaseQuantity { get; set; }

        public decimal Price { get; set; }

        public decimal Cost { get; set; }

        public decimal BaseRewards { get; set; }

        public DateTime RewardsExpireDate { get; set; }

        [Required]
        public int StatusID { get; set; }

        [Required, MaxLength(100)]
        public string StatusName { get; set; }

        [Required]
        public bool IsDefault { get; set; }

        [Required]
        public DateTime DateModified { get; set; }

        [Required]
        public int ModifiedBy { get; set; }

        [Required]
        public string ModifiedByName { get; set; }

    }
}
