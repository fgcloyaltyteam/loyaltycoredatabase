﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class PurchaseCatalogProduct : BaseEntity
    {
        public PurchaseCatalogProduct()
        {
            
        }

        public int ID { get; set; }

        [Required]
        public int CatalogID { get; set; }

        [Required]
        public int ProductID { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }
        
    }
}
