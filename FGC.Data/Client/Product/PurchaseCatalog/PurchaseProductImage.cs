﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class PurchaseProductImage : BaseEntity
    {
        public PurchaseProductImage()
        {
            
        }
        public int ID { get; set; }

        [Required]
        public int ProductID { get; set; }

        /// <summary>
        /// Image Type: Appropriate value from Option table. Example: Thumbnail, Main etc
        /// </summary>
        [Required]
        public int ImageTypeID { get; set; }

        [Required, MaxLength(100)]
        public string ImageName { get; set; }
    }
}
