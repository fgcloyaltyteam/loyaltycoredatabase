﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client.RawDataSource
{
    public class Member
    {
        public int ID { get; set; }
        public string LoginName { get; set; }
        public string ClientMemberID { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordHashCode { get; set; }

        public bool IsShowSurvey { get; set; }
        public bool CommunicateViaEmail { get; set; }
        public bool CommunicateViaSms { get; set; }
        public bool ReceiveNotification { get; set; }
        public bool ShowYourInformation { get; set; }

        public string CompanyName { get; set; }
        public string EmailAddress { get; set; }
        public string AlternateEmailAddress { get; set; }
        public string Phone { get; set; }
        public string AlternatePhone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }

        public int MemberStatusID { get; set; }
        public string MemberStatusName { get; set; }

        public int MemberTypeID { get; set; }
        public string MemberTypeName { get; set; }

        public string ImageFileName { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string MemberNotes { get; set; }
        public int HowDidYouHearID { get; set; }
        public string HowDidYouHearName { get; set; }

        public int MaritalStatusID { get; set; }
        public string MaritalStatusName { get; set; }
        public int NoOfFamilyMembers { get; set; }
        public int NoOfMaleMembers { get; set; }
        public int NoOfFemaleMembers { get; set; }
        public int NoOfKids { get; set; }
        public int IncomeID { get; set; }
        public string IncomeName { get; set; }

        public decimal Earned { get; set; }
        public decimal Redeemed { get; set; }
        public decimal Returned { get; set; }
        public decimal Balance { get; set; }
        public decimal TransferredOut { get; set; }
        public decimal TransferredIn { get; set; }

        public string partnerMemberID { get; set; }
        public string AccessToken { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public int TitleID { get; set; }
        public string TitleName { get; set; }
        public DateTime DOB { get; set; }
        public int ProfileTypeID { get; set; }
        public string ProfileTypeName { get; set; }
        public int SegmentTypeID { get; set; }
        public string SegmentTypeName { get; set; }
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public int GenderID { get; set; }
        public string GenderName { get; set; }

        public string HomeAddressLine1 { get; set; }
        public string HomeAddressLine2 { get; set; }
        public string HomeAddressLine3 { get; set; }
        public string HomeCity { get; set; }
        public int HomeProvinceID { get; set; }
        public string HomeProvinceName { get; set; }
        public int HomeCountryID { get; set; }
        public string HomeCountryName { get; set; }
        public string HomePostalCode { get; set; }

        public string OfficeAddressLine1 { get; set; }
        public string OfficeAddressLine2 { get; set; }
        public string OfficeAddressLine3 { get; set; }
        public string OfficeCity { get; set; }
        public int OfficeProvinceID { get; set; }
        public string OfficeProvinceName { get; set; }
        public int OfficeCountryID { get; set; }
        public string OfficeCountryName { get; set; }
        public string OfficePostalCode { get; set; }

        public string BillingAddressLine1 { get; set; }
        public string BillingAddressLine2 { get; set; }
        public string BillingAddressLine3 { get; set; }
        public string BillingCity { get; set; }
        public int BillingProvinceID { get; set; }
        public string BillingProvinceName { get; set; }
        public int BillingCountryID { get; set; }
        public string BillingCountryName { get; set; }
        public string BillingPostalCode { get; set; }

        public string ShippingAddressLine1 { get; set; }
        public string ShippingAddressLine2 { get; set; }
        public string ShippingAddressLine3 { get; set; }
        public string ShippingCity { get; set; }
        public int ShippingProvinceID { get; set; }
        public string ShippingProvinceName { get; set; }
        public int ShippingCountryID { get; set; }
        public string ShippingCountryName { get; set; }
        public string ShippingPostalCode { get; set; }

        public int RegionID { get; set; }
        public string RegionName { get; set; }

        public DateTime JoiningDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public DateTime ApprovalDate { get; set; }
        public DateTime DeactivationDate { get; set; }
        public string IpAddress { get; set; }
        public DateTime RenewedDate { get; set; }
        public DateTime ActivationDate { get; set; }

        public int AdvisorID { get; set; }
        public string AdvisorName { get; set; }

        public int QuestionID { get; set; }
        public string QuestionName { get; set; }
        public string Answer { get; set; }

        public int TerritoryID { get; set; }
        public string TerritoryName { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string ChangeSource { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

    }
}
