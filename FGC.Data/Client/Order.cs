﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class Order : BaseEntity
    {
        public int ID { get; set; }
        public int CartID { get; set; }
        public int MemberID { get; set; }
        public int OrderStatusID { get; set; }
        public decimal OrderNetTotal { get; set; }
        public decimal OrderGrossTotal { get; set; }
        public decimal OrderTax { get; set; }
        public decimal OrderShippingAmount { get; set; }
        public int OrderTotalPoints { get; set; }
    }
}
