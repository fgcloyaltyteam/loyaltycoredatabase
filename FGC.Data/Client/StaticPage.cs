﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class StaticPage : BaseEntity
    {
        public int ID { get; set; }

        [Required]
        public string PageTitle { get; set; }
        
        public string PageContent { get; set; }
      
    }
}
