﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client.TypeSpecific
{
    public class MemberDesignation : BaseEntity
    {
        public MemberDesignation()
        {
            Member = new Member();
            Option = new Option();
        }
        public int ID { get; set; }

        [Required]
        [ForeignKey("Member")]
        [Column(Order = 1)]
        public int MemberID { get; set; }

        [Required]
        public int designationID { get; set; }



        public virtual Member Member { get; set; }
        public virtual Option Option { get; set; }
    }
}
