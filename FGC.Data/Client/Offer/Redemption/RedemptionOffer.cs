﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class RedemptionOffer : BaseEntity
    {
        public int ID { get; set; }

        [Required, MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public DateTime DateApproved { get; set; }

        public int ApprovedBy { get; set; }

        [MaxLength(500)]
        public string ModifyReason { get; set; }

        [Required]
        public int StatusID { get; set; }




    }
}
