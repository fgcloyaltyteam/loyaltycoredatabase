﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class PurchaseOfferProduct : BaseEntity
    {
        public PurchaseOfferProduct()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int OfferID { get; set; }

        [Required]
        public int ProductID { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public int RewardPoints { get; set; }
    }
}
