﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class RedemptionOrderDetail : BaseEntity
    {
        public RedemptionOrderDetail()
        {
            IsGift = false;
            GiftWrap = false;
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int OrderID { get; set; }

        [Required]
        public int ProductID { get; set; }

        [Required]
        public int VariantID { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public int PointsRedeemed { get; set; }

        public int ShippingMethodID { get; set; }

        public int ShippingStatusID { get; set; }

        public DateTime ShippingDate { get; set; }

        public DateTime PickupDate { get; set; }

        public DateTime DeliveryDate { get; set; }

        public int FulfillmentID { get; set; }

        public int CarrierID { get; set; }

        [MaxLength(20)]
        public string TrackingNo { get; set; }

        [MaxLength(200)]
        public string Trackinglink { get; set; }

        public bool IsGift { get; set; }

        public bool GiftWrap { get; set; }

        [MaxLength(500)]
        public string GiftMessage { get; set; }

        [MaxLength(200)]
        public string ShippingInstruction { get; set; }

        public int PackagingOptionID { get; set; }

        [MaxLength(500)]
        public string ShipmentNotification { get; set; }

        public int QuantityShipped { get; set; }

    }
}
