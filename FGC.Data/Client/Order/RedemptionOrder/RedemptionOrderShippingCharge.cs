﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class RedemptionOrderShippingCharge : BaseEntity
    {
        public RedemptionOrderShippingCharge()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int OrderID { get; set; }

        [Required]
        public int ChargeTypeID { get; set; }

        [Required]
        public decimal ChargedAmount { get; set; }


    }
}
