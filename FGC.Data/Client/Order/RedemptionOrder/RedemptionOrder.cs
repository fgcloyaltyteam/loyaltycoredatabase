﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class RedemptionOrder : BaseEntity
    {
        public RedemptionOrder()
        {
            IsPartialShipmentAllowed = false;
            SendEmailStatus = false;
            BillingShippingSame = true;
        }

        [Key]
        public int ID { get; set; }

        public int cartID { get; set; }

        /// <summary>
        /// Order Method: Online, Phone...  Appropriate ID from Option
        /// </summary>
        [Required]
        public int MethodID { get; set; }

        [Required]
        public int MemberID { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public bool IsPartialShipmentAllowed { get; set; }

        [MaxLength(20)]
        public string OrderNumber { get; set; }

        /// <summary>
        /// Order Type, appropriate value from Option table...  Online, in-store, phone etc.
        /// </summary>
        [Required]
        public int TypeID { get; set; }

        [Required]
        public int NoOfItems { get; set; }

        [Required]
        public int StatusID { get; set; }

        [Required]
        public bool SendEmailStatus { get; set; }

        [Required]
        public bool BillingShippingSame { get; set; }

        //// Member Instruction / Comments
        //[MaxLength(200)]
        //public string Comments { get; set; }

        //[MaxLength(200)]
        //public string PublicComments { get; set; }

        [MaxLength(50)]
        public string QRCode { get; set; }
    }
}
