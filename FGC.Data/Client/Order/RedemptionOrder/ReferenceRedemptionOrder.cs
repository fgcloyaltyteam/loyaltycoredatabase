﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ReferenceRedemptionOrder : BaseEntity
    {
        public ReferenceRedemptionOrder()
        {

        }

        /// <summary>
        /// Order Reference ID
        /// </summary>
        [Key]
        public int ID { get; set; }

        [Required]
        public int ShoppingCartID { get; set; }

        [Required]
        public int MemberID { get; set; }

        [MaxLength(50)]
        public string ShoppingCartSessionID { get; set; }

        [Required]
        public int Status { get; set; } 
    }
}
