﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class RedemptionOrderBillingInfo : BaseEntity
    {
        // Billing Information
        public RedemptionOrderBillingInfo()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int OrderID { get; set; }

        [MaxLength(200)]
        public string BillingCompanyName { get; set; }

        [Required, MaxLength(50)]
        public string BillingPersonFirstName { get; set; }

        [Required, MaxLength(50)]
        public string BillingPersonLastName { get; set; }

        [Required, MaxLength(200)]
        public string BillingAddressLine1 { get; set; }

        [MaxLength(200)]
        public string BillingAddressLine2 { get; set; }

        [MaxLength(200)]
        public string BillingAddressLine3 { get; set; }

        [Required, MaxLength(100)]
        public string BillingCity { get; set; }

        [Required]
        public int BillingStateProvinceID { get; set; }

        [Required]
        public int BillingCountryID { get; set; }

        [Required, MaxLength(20)]
        public string BillingPostalZipCode { get; set; }

        [MaxLength(20)]
        public string BillingPersonPhone { get; set; }

        [MaxLength(100)]
        public string BillingPersonEmail { get; set; }
    }
}
