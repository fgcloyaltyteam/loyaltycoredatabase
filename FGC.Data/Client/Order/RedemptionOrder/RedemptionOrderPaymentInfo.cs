﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class RedemptionOrderPaymentInfo : BaseEntity
    {
        // Redemption Order Payment Information
        public RedemptionOrderPaymentInfo()
        {
            
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int OrderID { get; set; }

        [MaxLength(20)]
        public string DiscountCode { get; set; }

        public int Discount { get; set; }

        [Required]
        public int PointsRedeemed { get; set; }

        public decimal AmountPaid { get; set; }

    }
}
