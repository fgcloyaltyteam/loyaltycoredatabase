﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class OrderDetail : BaseEntity
    {
        public OrderDetail()
        {

        }

        public int ID { get; set; }

        [Required]
        public int OrderID { get; set; }

        [Required]
        public int ProductID { get; set; }

        [Required]
        public int VariantID { get; set; }

        public int ItemTypeID { get; set; }

        [Required]
        public int ItemQuantity { get; set; }

        public int ItemQuantityMultiplier { get; set; }

        [Required]
        public decimal ItemRetailCost { get; set; }

        public bool NoTaxFlag { get; set; }


        // Item Shipping Information 
        public int shippingMethodID { get; set; }

        [Required]
        public int ShippingStatusID { get; set; }

        public DateTime ShippingDate { get; set; }

        public DateTime PickupDate { get; set; }

        public DateTime DeliveryDate { get; set; }

        public bool SignatureRequired { get; set; }

        public int FulfillmentCenterID { get; set; }

        public int CarrierID { get; set; }

        [MaxLength(20)]
        public string TrackingNo { get; set; }

        [MaxLength(200)]
        public string Trackinglink { get; set; }

        public bool IsGift { get; set; }

        public bool GiftWrap { get; set; }

        [MaxLength(500)]
        public string GiftMessage { get; set; }

        [MaxLength(200)]
        public string ShippingInstruction { get; set; }

        public int packagingOptionID { get; set; }

        [MaxLength(500)]
        public string ShipmentNotification { get; set; }

        public int ItemQuantityShipped { get; set; }

        public decimal ItemTaxableAmount { get; set; }

        public decimal ItemTaxAmount { get; set; }

        public bool HBR { get; set; }

    }
}
