﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ReferenceOrderDetail: BaseEntity
    {
        public ReferenceOrderDetail()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int ReferenceOrderID { get; set; }

        /// <summary>
        /// One Tab = One MenuID, for example Order Detail, Order Items, Billing Info, Shipping Info etc.
        /// </summary>
        [Required]
        public int MenuID { get; set; }

        /// <summary>
        /// JSON Object of Order Detail, Order Items, Billing Info, Shipping Info etc.
        /// </summary>
        [Required]
        public string Content { get; set; }
    }
}
