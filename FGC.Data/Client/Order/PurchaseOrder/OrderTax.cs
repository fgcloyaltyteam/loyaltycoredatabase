﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class OrderTax: BaseEntity
    {
        public OrderTax()
        {
           
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int OrderID { get; set; }

        [Required]
        public int TaxTypeID { get; set; }

        [Required]
        public decimal TaxAmount { get; set; }

    }
}
