﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class OrderShippingInfo : BaseEntity
    {
        public OrderShippingInfo()
        {
            
        }

        // Shipping Information
        public int ID { get; set; }

        [Required]
        public int OrderID { get; set; }

        [MaxLength(200)]
        public string ShippingCompanyName { get; set; }

        [Required, MaxLength(50)]
        public string ContactPersonFirstName { get; set; }

        [Required, MaxLength(50)]
        public string ContactPersonLastName { get; set; }

        [Required, MaxLength(200)]
        public string ShippingAddressLine1 { get; set; }

        [MaxLength(200)]
        public string ShippingAddressLine2 { get; set; }

        [MaxLength(200)]
        public string ShippingAddressLine3 { get; set; }

        [Required, MaxLength(100)]
        public string ShippingCity { get; set; }

        [Required]
        public int ShippingStateID { get; set; }

        [Required]
        public int ShippingCountryID { get; set; }

        [Required, MaxLength(20)]
        public string ShippingPostalCode { get; set; }

        [MaxLength(20)]
        public string ContactPersonPhone { get; set; }

        [MaxLength(100)]
        public string ContactPersonEmail { get; set; }

        public decimal TotalWeight { get; set; }

        [MaxLength(200)]
        public string Instructions { get; set; }

    }
}
