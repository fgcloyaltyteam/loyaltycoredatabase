﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class OrderPaymentSummary : BaseEntity
    {
        // Payment Summary Information

        public OrderPaymentSummary()
        {
            
        }

        public int ID { get; set; }

        [Required]
        public int OrderID { get; set; }

        [Required]
        public decimal NetTotal { get; set; }

        public decimal ShippingAmount { get; set; }

        public decimal DiscountAmount { get; set; }

        public decimal MemberCredit { get; set; }

        public decimal TaxableAmount { get; set; }

        public decimal TaxAmount { get; set; }

        public decimal OtherCharge { get; set; }

        public decimal AdditionalFees { get; set; }

        [Required]
        public decimal GrossTotal { get; set; }
        
    }
}
