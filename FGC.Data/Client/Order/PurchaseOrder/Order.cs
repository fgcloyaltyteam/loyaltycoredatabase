﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class Order : BaseEntity
    {
        public Order()
        {
            IsMultipleShippers = false;
            SendEmailStatus = false;
            BillingShippingSame = true;
        }

        [Key]
        public int ID { get; set; }

        public int cartID { get; set; }

        /// <summary>
        /// Order Channel: Online, Backoffice, In-Store etc., Appropriate ID from Option
        /// </summary>
        public int ChannelID { get; set; }

        [Required]
        public int MemberID { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public bool IsMultipleShippers { get; set; }

        public string OrderNumber { get; set; }

        /// <summary>
        /// Order Type, appropriate value from Option table...  Online, in-store, phone etc.
        /// </summary>
        public int TypeID { get; set; }

        [Required]
        public int NoOfItems { get; set; }

        public int StatusID { get; set; }

        public bool SendEmailStatus { get; set; }

        public bool BillingShippingSame { get; set; }

        // Could be a thired party order number
        public string partnerOrderNumber { get; set; }

        // Member Instruction / Comments
        [MaxLength(200)]
        public string Comments { get; set; }

        [MaxLength(200)]
        public string PublicComments { get; set; }


        // Commission 
        [MaxLength(20)]
        public string salesRepCode { get; set; }

        [MaxLength(20)]
        public string assignedSalesRepCode { get; set; }

        // Other Information 
        [MaxLength(100)]
        public string orderLocation { get; set; }

        [MaxLength(100)]
        public string AffiliateCameFrom { get; set; }

        [MaxLength(100)]
        public string TrafficSource { get; set; }

        [MaxLength(100)]
        public string TrafficSearchPhrase { get; set; }

        public bool PayPerClickOrder { get; set; }

        [MaxLength(100)]
        public string PayPerClickKeywordUsed { get; set; }

        public decimal GrossAmount { get; set; }

        public decimal AmountPaid { get; set; }

        [MaxLength(20)]
        public string PONumber { get; set; }

        public bool POPaid { get; set; }

        [MaxLength(20)]
        public string TPInvoiceNumber { get; set; }

        public DateTime TPInvoiceDate { get; set; }

        public int LanguageID { get; set; }
        
        public int BonusGiftID { get; set; }

        [MaxLength(50)]
        public string QRCode { get; set; }

    }
}
