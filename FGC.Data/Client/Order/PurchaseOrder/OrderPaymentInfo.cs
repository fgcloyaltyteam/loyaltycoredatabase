﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class OrderPaymentInfo: BaseEntity
    {
        // Payment Information
        public OrderPaymentInfo()
        {
           
        }

        public int ID { get; set; }

        [Required]
        public int OrderID { get; set; }

        public int CurrencyID { get; set; }

        // Payment Type: COD, Credit Card, Debit Card, NetBanking, Cheque, Payment Against PO etc.
        public int TypeID { get; set; }

        // Payment Gateway ID
        public int GatewayID { get; set; }

        // Payment gateway name
        public string GatewayName { get; set; }

        public decimal ApprovedPaymentTotal { get; set; }

        [MaxLength(100)]
        public string AuthorizationCode { get; set; }


        public int CCTypeID { get; set; }

        [MaxLength(50)]
        public string CCNumber { get; set; }

        public int CCExpMonth { get; set; }

        public int CCExpYear { get; set; }

        [MaxLength(10)]
        public string CCCVV { get; set; }

        [MaxLength(100)]
        public string CCName { get; set; }

        [MaxLength(50)]
        public string CCAVSResponse { get; set; }

        [MaxLength(50)]
        public string Method { get; set; }

        [MaxLength(20)]
        public string RejectReasonCode { get; set; }

        [MaxLength(200)]
        public string RejectReasonMessage { get; set; }


        public int StatusID { get; set; }

        [MaxLength(10)]
        public string ChequeNumber { get; set; }

        public DateTime ChequeDate { get; set; }

        public decimal ChequeAmount { get; set; }

        public string PONumber { get; set; }

        public decimal POAmount { get; set; }

    }
}
