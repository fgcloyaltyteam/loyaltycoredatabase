﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ReferenceOrder : BaseEntity
    {
        public ReferenceOrder()
        {

        }

        /// <summary>
        /// Order Reference ID
        /// </summary>
        [Key]
        public int ID { get; set; }

        [Required]
        public int ShoppingCartID { get; set; }

        [Required, MaxLength(50)]
        public string ShoppingCartSessionID { get; set; }

        [Required]
        public int Status { get; set; }
    }
}
