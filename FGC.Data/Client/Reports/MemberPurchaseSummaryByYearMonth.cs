﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class MemberPurchaseSummaryByYearMonth
    {
        public int ID { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int MemberID { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int TotalQuantityPurchased { get; set; }
        public int TotalPointsEarned { get; set; }
        public int AveragePointsPerUnit { get; set; }

    }
}
