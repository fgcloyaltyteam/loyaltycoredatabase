﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ReportSubGroup: BaseEntity
    {
        public ReportSubGroup()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int GroupID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

    }
}
