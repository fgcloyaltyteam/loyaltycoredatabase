﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class Report: BaseEntity
    {
        public Report()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int TableID { get; set; }

        [Required]
        public int SubGroupID { get; set; }

        [Required, MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// Web API URL
        /// </summary>
        public string SourceURL { get; set; }
    }
}
