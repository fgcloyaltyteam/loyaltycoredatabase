﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ReportTableColumn: BaseEntity
    {
        public ReportTableColumn()
        {
            IsSearchable = false;
            ControlTypeID = 1;
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int TableID { get; set; }

        [Required]
        public string ColumnName { get; set; }

        // Need to handel from CMS side from Physical Table Columns
        //[Required]
        //public string ColumnLabel { get; set; }

        [Required]
        public bool IsSearchable { get; set; }

        [Required]
        public int ControlTypeID { get; set; }
    }
}
