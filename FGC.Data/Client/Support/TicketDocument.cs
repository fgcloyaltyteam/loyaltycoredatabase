﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class TicketDocument: BaseEntity
    {
        public TicketDocument()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int TicketID { get; set; }

        [Required, MaxLength(200)]
        public string Name { get; set; }



    }
}
