﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class Ticket: BaseEntity
    {
        public Ticket()
        {

        }

        [Key]
        public int ID { get; set; }

        public int MemberID { get; set; }

        public string TicketReferenceID { get; set; }

        /// <summary>
        /// Appropriate ID from Options: Ticket Category
        /// </summary>
        [Required]
        public int CategoryID { get; set; }

        /// <summary>
        /// Appropriate ID from Options: Ticket Reason
        /// </summary>
        [Required]
        public int ReasonID { get; set; }

        [Required, MaxLength(200)]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Appropriate ID from Options: Ticket Status
        /// </summary>
        [Required]
        public int Status { get; set; }

        public int ClosedBy { get; set; }

        public DateTime? DateClosed { get; set; }

        

    }
}
