﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class OrderDetail : BaseEntity
    {
        public int ID { get; set; }

        [ForeignKey("Order")]
        public int OrderID { get; set; }
        public int ProductID { get; set; } 
        public int Quantity { get; set; }
        public decimal CostPrice { get; set; }
        public decimal Price { get; set; }
        public int RedeemPoint { get; set; }

        public virtual Order Order { get; set; }
    }
}
