﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Client
{
    public class Entities : BaseEntity
    {
        public int ID { get; set; }

        [Required]
        public string EntityName { get; set; }
    }
}
