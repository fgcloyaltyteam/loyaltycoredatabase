﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class StaticPage : BaseEntity
    {
        public int ID { get; set; }

        [Required, MaxLength(100)]
        public string Code { get; set; }

        [Required]
        public bool StatusID { get; set; }
    }
}
