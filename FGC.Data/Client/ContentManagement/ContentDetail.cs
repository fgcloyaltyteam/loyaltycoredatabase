﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class ContentDetail : BaseEntity
    {
        public ContentDetail()
        {

        }
        public int ID { get; set; }

        // Code will be stored here, this code will same as referencing master data tables like Product Name Code from Product table
        [Required, MaxLength(100)]
        public string Code { get; set; }

        // Language specific Text for above Code will be stored here, like Product Name for Product Master table
        [Required]
        public string Text { get; set; }

        [Required]
        public int LanguageID { get; set; }

        [Required]
        public int ChannelID { get; set; }

        [Required]
        public bool StatusID { get; set; }

        [Required]
        public int ModuleID { get; set; }




    }
}
