﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class Partner : BaseEntity
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// Appropriate value from Options table
        /// Brand of Merchant
        /// </summary>
        [Required]
        public int TypeID { get; set; }



    }
}
