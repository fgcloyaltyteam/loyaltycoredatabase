﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.Data.Client
{
    public class PartnerCategory : BaseEntity
    {
        public int ID { get; set; }

        [Required]
        public int PartnerID { get; set; }

        [Required]
        public int CategoryID { get; set; }

    }
}
