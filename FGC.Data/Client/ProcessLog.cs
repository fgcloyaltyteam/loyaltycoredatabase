﻿ 
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations;


namespace FGC.Data.Client
{
    public class ProcessLog : BaseEntity
    {
        public ProcessLog()
        {
             
        }

        public int ID { get; set; }
        [MaxLength(100)]
        public string UserID { get; set; }
        public string Application { get; set; }
        public string Log  { get; set; } 
        public string Module { get; set; }
        [MaxLength(10)]
        public string ProcessId { get; set; }
        public string Comments { get; set; }
    }
}