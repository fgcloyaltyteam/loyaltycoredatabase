﻿using FGC.Data.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("ApiRoutes")]
    public class ApiRoutes : BaseEntity
    {
        public ApiRoutes()
        {
        }
        [Key]
        public int ID { get; set; }
        [Required, MaxLength(100)]
        public string SourcePortal { get; set; }
        public string SourceSite { get; set; }
        public string SourceController { get; set; }
        public string SourceAction { get; set; }
        //public string SourceParmeters { get; set; }

        public string TargetPortal { get; set; }
        public string TargetSite { get; set; }
        public string TargetController { get; set; }
        public string TargetAction { get; set; }
        //public string TargetParameters { get; set; }
    }
}