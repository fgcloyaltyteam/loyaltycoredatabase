﻿using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("ApiRoutesParameter")]
    public class ApiRoutesParameter : BaseEntity
    {
        public ApiRoutesParameter()
        {
        }

        public int ID { get; set; }
        public string ApiRouteID { get; set; } 
        public string SourceParmeters { get; set; }        
        public string TargetParameters { get; set; }
    }
}