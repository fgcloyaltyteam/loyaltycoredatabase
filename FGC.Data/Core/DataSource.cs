﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("DataSource")]
    public class DataSource 
    {
        public DataSource()
        {  
        }
        [Key]
        public int ID { get; set; }
        public  string Name { get; set; }
        public string ApiName { get; set; }
        public string CredentialTemplate { get; set; }
        public string Credentials { get; set; }
    }
}
