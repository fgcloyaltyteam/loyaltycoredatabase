﻿using System.ComponentModel.DataAnnotations;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("Program")]
    public class Program : BaseEntity
    {
        public Program()
        {

        }
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        //public int ClientID { get; set; } 
    }
}

