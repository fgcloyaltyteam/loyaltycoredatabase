﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;

namespace FGC.CoreData
{
    [Table("PreRegistration")]
    public class PreRegistration : BaseEntity
    {
        public PreRegistration()
        {
            this.Token = base.RowID.ToString().Replace("-","");
        }
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public string RegistrationJson { get; set; }
        public string SubscriptionJson { get; set; }
        public string Token { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}