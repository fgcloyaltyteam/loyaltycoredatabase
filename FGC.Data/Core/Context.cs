﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.CoreData;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Collections.Concurrent;

namespace FGC.Data
{
    public class CoreContext : DbContext, IDisposable
    {

        private static ConcurrentDictionary<Tuple<string, string>, DbCompiledModel> modelCache
            = new ConcurrentDictionary<Tuple<string, string>, DbCompiledModel>();

        #region DbSet
        //public virtual DbSet<ApiToken> ApiToken { get; set; }
        //public virtual DbSet<ApiRoutes> ApiRoutes { get; set; }
        //public virtual DbSet<ApiRoutesParameter> ApiRoutesParameter { get; set; }

        public DbSet<ClientDataSource> ClientDataSource { get; set; }
        public DbSet<ClientProgram> ClientProgram { get; set; }
        public DbSet<ClientServices> ClientServices { get; set; }
        public DbSet<Service> Service { get; set; }
        public DbSet<PortalServices> PortalServices { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<ServiceType> ServiceType { get; set; }

        public virtual DbSet<GlobalServer> GlobalServer { get; set; }
        public virtual DbSet<GlobalUser> GlobalUser { get; set; }
        public DbSet<Portal> Portal { get; set; }
        public DbSet<PortalType> PortalType { get; set; }
        public virtual DbSet<Program> Program { get; set; }
        public DbSet<ProgramWebsite> ProgramWebsite { get; set; }
        public DbSet<Province> Province { get; set; }
        public DbSet<ServerType> ServerType { get; set; }
        public DbSet<PreRegistration> PreRegistration { get; set; }

        // Core
        public DbSet<BillingCycle> BillingCycle { get; set; }
        public DbSet<ClientPackage> ClientPackage { get; set; }
        public DbSet<FGC.CoreData.ClientPackageDetail> ClientPackageDetail { get; set; }
        public DbSet<Feature> Feature { get; set; }
        public DbSet<FeatureModule> FeatureModule { get; set; }
        public DbSet<Package> Package { get; set; }
        public DbSet<PackageFeature> PackageFeature { get; set; }
        //public DbSet<ProgramPackage> ProgramPackage { get; set; }
        public DbSet<CompanyAddress> CompanyAddress { get; set; }

        public DbSet<DataSource> DataSource { get; set; }

        public DbSet<Option> Option { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<ViewModel> ViewModel { get; set; }
        public DbSet<ViewModelDetail> ViewModelDetail { get; set; }
        public DbSet<ViewModelLanguageDetail> ViewModelLanguageDetail { get; set; }


        // CSS Theme
        public DbSet<CSSLayout> CSSLayout { get; set; }
        public DbSet<CSSRepositoryThemeVariableValue> CSSRepositoryThemeVariableValue { get; set; }
        public DbSet<CSSTheme> CSSTheme { get; set; }
        public DbSet<CSSThemeRepository> CSSThemeRepository { get; set; }
        public DbSet<CSSVariable> CSSVariable { get; set; }
        public DbSet<CSSVariableDefaultValue> CSSVariableDefaultValue { get; set; }
        public DbSet<CSSThemeVariableValue> CSSThemeVariableValue { get; set; }
        public DbSet<CSSWebsiteTheme> CSSWebsiteTheme { get; set; }
        public DbSet<Technology> Technology { get; set; }
        public  DbSet<PortalTechnology> PortalTechnology { get; set; }
        public DbSet<ServiceResources> ServiceResources { get; set; }
        //public DbSet<ProgramService> ProgramService { get; set; } 
        public DbSet<ClientSubscriptions> ClientSubscriptions { get; set; }

        #endregion

        public CoreContext() : base("name=LoyaltyDBContext")
        {
            // Database.SetInitializer(new MigrateDatabaseToLatestVersion<Context, FGC.Data.Migrations.Configuration>());
            Database.SetInitializer(new CreateDatabaseIfNotExists<CoreContext>());
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);


        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        //    modelBuilder.Entity<Feature>().Property(x => x.MenuID).IsOptional();

        //    modelBuilder.Entity<Option>().Property(x => x.IsDefault).IsOptional();
        //    modelBuilder.Entity<Option>().Property(x => x.ParentID).IsOptional();

        //    modelBuilder.Entity<ViewModel>().Property(x => x.ViewCSS).IsOptional();
        //    modelBuilder.Entity<ViewModel>().Property(x => x.Entity).IsOptional();
        //    modelBuilder.Entity<ViewModel>().Property(x => x.MetaDescription).IsOptional();
        //    modelBuilder.Entity<ViewModel>().Property(x => x.MetaKeyword).IsOptional();
        //    modelBuilder.Entity<ViewModel>().Property(x => x.MetaTitle).IsOptional();

        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.FieldLabelName).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.Name).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.ControlType).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.UIServingClassName).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.CSSClassName).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.LabelCSSClassName).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.Placeholder).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.DropDownJson).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.Help).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.ValidationMessage).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.SourceEntity).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.AllowSearching).IsOptional();
        //    modelBuilder.Entity<ViewModelDetail>().Property(x => x.Display).IsOptional();

        //    modelBuilder.Entity<ViewModelLanguageDetail>().Property(x => x.FieldLabelName).IsOptional();
        //    modelBuilder.Entity<ViewModelLanguageDetail>().Property(x => x.Help).IsOptional();
        //    modelBuilder.Entity<ViewModelLanguageDetail>().Property(x => x.ValidationMessage).IsOptional();
        //    modelBuilder.Entity<ViewModelLanguageDetail>().Property(x => x.Placeholder).IsOptional();
        //    modelBuilder.Entity<ViewModelLanguageDetail>().Property(x => x.DatabaselName).IsOptional();

        //    modelBuilder.Entity<Technology>().Property(x => x.Code).IsOptional();

        //    modelBuilder.Entity<Package>().Property(x => x.PackageTechnology).IsOptional();
        //    modelBuilder.Entity<Package>().Property(x => x.PackagePortal).IsOptional();

        //    modelBuilder.Entity<Portal>().Property(x => x.ProgramID).IsOptional();

        //    modelBuilder.Entity<Feature>().Property(x => x.FeatureParent).IsOptional();
        //    modelBuilder.Entity<ProgramWebsite>().Property(x => x.PortalID).IsOptional();

        //    modelBuilder.Entity<Menu>().Property(x => x.PortalID).IsOptional();
        //    modelBuilder.Entity<Menu>().Property(x => x.ContentTypeID).IsOptional();
        //}



        public static CoreContext Create(/*Guid schema,*/ DbConnection connection)
        {

            var compiledModel = modelCache.GetOrAdd(
                Tuple.Create(connection.ConnectionString, ""), // schema.ToString()
                t =>
                {
                    try
                    {
                        var modelBuilder = new DbModelBuilder();

                        #region Entity Configuration
                        // Location
                        //builder.Entity<Location>().Property(x => x.AddressLine1).IsOptional();
                        //builder.Entity<Location>().Property(x => x.AddressLine2).IsOptional();
                        modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

                        modelBuilder.Entity<Feature>().Property(x => x.MenuID).IsOptional();

                        modelBuilder.Entity<Option>().Property(x => x.IsDefault).IsOptional();
                        modelBuilder.Entity<Option>().Property(x => x.ParentID).IsOptional();

                        modelBuilder.Entity<ViewModel>().Property(x => x.ViewCSS).IsOptional();
                        modelBuilder.Entity<ViewModel>().Property(x => x.Entity).IsOptional();
                        modelBuilder.Entity<ViewModel>().Property(x => x.MetaDescription).IsOptional();
                        modelBuilder.Entity<ViewModel>().Property(x => x.MetaKeyword).IsOptional();
                        modelBuilder.Entity<ViewModel>().Property(x => x.MetaTitle).IsOptional();

                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.FieldLabelName).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.Name).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.ControlType).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.UIServingClassName).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.CSSClassName).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.LabelCSSClassName).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.Placeholder).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.DropDownJson).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.Help).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.ValidationMessage).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.SourceEntity).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.AllowSearching).IsOptional();
                        modelBuilder.Entity<ViewModelDetail>().Property(x => x.Display).IsOptional();

                        modelBuilder.Entity<ViewModelLanguageDetail>().Property(x => x.FieldLabelName).IsOptional();
                        modelBuilder.Entity<ViewModelLanguageDetail>().Property(x => x.Help).IsOptional();
                        modelBuilder.Entity<ViewModelLanguageDetail>().Property(x => x.ValidationMessage).IsOptional();
                        modelBuilder.Entity<ViewModelLanguageDetail>().Property(x => x.Placeholder).IsOptional();
                        modelBuilder.Entity<ViewModelLanguageDetail>().Property(x => x.DatabaselName).IsOptional();

                        modelBuilder.Entity<Technology>().Property(x => x.Code).IsOptional();

                        modelBuilder.Entity<Package>().Property(x => x.PackageTechnology).IsOptional();
                        modelBuilder.Entity<Package>().Property(x => x.PackagePortal).IsOptional();

                        modelBuilder.Entity<Portal>().Property(x => x.ProgramID).IsOptional();

                        modelBuilder.Entity<Feature>().Property(x => x.FeatureParent).IsOptional();
                        modelBuilder.Entity<ProgramWebsite>().Property(x => x.PortalID).IsOptional();

                        modelBuilder.Entity<Menu>().Property(x => x.PortalID).IsOptional();
                        modelBuilder.Entity<Menu>().Property(x => x.ContentTypeID).IsOptional();

                        #endregion
                        try
                        {
                            var model = modelBuilder.Build(connection);
                            return model.Compile();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }

                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                });

            var context = new CoreContext() ; // compiledModel);
            context.Configuration.LazyLoadingEnabled = true;
            context.Configuration.ProxyCreationEnabled = true;
            return context;
        }

        public static void InitializeTenant(/*Guid schema,*/ DbConnection connection)
        {
            using (var _ctx = Create(/*schema,*/ connection))
            {
                if (!_ctx.Database.Exists())
                {
                    _ctx.Database.Create();
                }
                else
                {
                    Database.SetInitializer<CoreContext>(null);
                    var createScript = ((IObjectContextAdapter)_ctx).ObjectContext.CreateDatabaseScript();
                    _ctx.Database.ExecuteSqlCommand(createScript); 
                }
                  
            }
        }


    }
}
