﻿using FGC.Data.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("ApiToken")]
    public class ApiToken : BaseEntity
    {
        public ApiToken()
        {
        }

        public int ID { get; set; }
        public Guid ClientKey { get; set; }
        public Guid TempToken { get; set; }
        public DateTime LastAccessed { get; set; }
        public DateTime Expire { get; set; }
    }
}