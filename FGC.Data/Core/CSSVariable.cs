﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData
{
    [Table("CSSVariable")]
    public class CSSVariable: BaseEntity
    {
        public CSSVariable()
        {
            ShowToClient = false;
        }

        [Key]
        public int ID { get; set; }
        
        [Required]
        public  int LayoutID { get; set; }

        [Required, MaxLength(100)]
        public string VariableCode { get; set; }

        [Required, MaxLength(100)]
        public  string VariableName { get; set; }

        [Required, DefaultValue("0")]
        public bool ShowToClient { get; set; }


    }
}
