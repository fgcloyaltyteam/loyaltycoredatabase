﻿using System;
using System.ComponentModel.DataAnnotations;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("GlobalServer")]
    public class GlobalServer : BaseEntity
    {
        public GlobalServer()
        {
            UserSchemaID = Guid.NewGuid();
        }
        public int ID { get; set; }

        [Required]
        public int GlobalUserID { get; set; }
        public string Name { get; set; }
        public int ServiceID { get; set; }
        public Guid UserSchemaID { get; set; }
        public string IPAddress { get; set; }
        public string SqlServerName { get; set; }
        public string Database { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int ServerType { get; set; }
        public string ConnectionString { get; set; }
        public int Priority { get; set; }
        public bool IsSiteOn  { get; set; } 

    }
}

