﻿using System;
using System.ComponentModel.DataAnnotations;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("ClientServices")]
    public class ClientServices : BaseEntity
    {
        public ClientServices()
        {

        }
        public int ID { get; set; }

        [Required]
        public int GlobalUserID { get; set; }
        [Required]
        public int ServiceID { get; set; }
        [Required]
        public int PackageID { get; set; }

        //public string IPAddress { get; set; }
        //public string SqlServerName { get; set; }
        //public string Database { get; set; }
        //public string UserName { get; set; }
        //public string Password { get; set; } 
        //public string ConnectionString { get; set; }

    }
}

