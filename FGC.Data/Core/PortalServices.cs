﻿using System;
using System.ComponentModel.DataAnnotations;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("PortalServices")]
    public class PortalServices : BaseEntity
    {
        public PortalServices()
        {

        }
        public int ID { get; set; }

       
        [Required]
        public int ServiceID { get; set; }
        [Required]
        public int PortalID { get; set; }

        public string ServiceRoute{ get; set; }

        public string  Function { get; set; }
    }
}

