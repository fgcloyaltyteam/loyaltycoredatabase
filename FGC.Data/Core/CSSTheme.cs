﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData
{
    [Table("CSSTheme")]

    public class CSSTheme : BaseEntity
    {
        public CSSTheme()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required, MaxLength(100)]
        public string ThemeName { get; set; }

        [Required]
        public int LayoutID { get; set; }

        [Required, MaxLength(100)]
        public string ThemeImage { get; set; }

        [Required, MaxLength(100)]
        public string ThemeHomeImage { get; set; }


    }
}
