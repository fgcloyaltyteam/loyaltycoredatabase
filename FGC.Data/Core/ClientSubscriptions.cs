﻿using System;
using System.ComponentModel.DataAnnotations;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("ClientSubscriptions")]
    public class ClientSubscriptions : BaseEntity
    {
        public ClientSubscriptions()
        {

        }
        public int ID { get; set; }

        [Required]
        public int GlobalUserID { get; set; }
        [Required]
        public int ServiceID { get; set; }
        [Required]
        public int PackageID { get; set; }
        public DateTime SubscriptionDate  { get; set; }
        public DateTime UnSubscriptionDate { get; set; } 
        public int SubscriptionStatus  { get; set; } 
    }
}

