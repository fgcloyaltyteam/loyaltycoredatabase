﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("ProgramService")]
    public class ProgramService : BaseEntity
    {
        public int ID { get; set; }

        public int ServiceID { get; set; }

        public int ProgramID { get; set; }
       

    }
}
