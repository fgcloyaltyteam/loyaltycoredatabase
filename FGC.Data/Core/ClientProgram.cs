﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;


namespace FGC.CoreData
{
    [Table("ClientProgram")]
    public class ClientProgram
    {
        public int ID { get; set; }

        public int GlobaluserID { get; set; }

        public int ProgramID { get; set; } 
        public int ServerID { get; set; }        
    }
}