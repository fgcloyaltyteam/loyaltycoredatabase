﻿using System; 
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;


namespace FGC.CoreData
{
    [Table("GlobalUser")]
    public class GlobalUser: BaseEntity
    {
        public GlobalUser()
        {
            //UserSchemaID = Guid.NewGuid();
            UserValidationKey =  Guid.NewGuid();
        }

        public int ID { get; set; } 
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid UserValidationKey { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordHashCode { get; set; } 
        public string ClientFirstName { get; set; }
        public string ClientLastName { get; set; } 
        public string ClientPhone { get; set; } //{"Invalid column name 'ClientPhone'."}
        public int ClientCode{ get; set; }

        //public string IsClientAdmin { get; set; }

        //public string IsSuperAdmin { get; set; }

        //[ForeignKey("GlobalServer")]
        //public int ServerID { get; set; }

        //public virtual GlobalServer GlobalServer { get; set; }

    }
}