﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("PortalTechnology")]
    public class PortalTechnology : BaseEntity
    {
        public int ID { get; set; } 
        public int PortalID  { get; set; }
        public int TechnologyID { get; set; }

    }
}
