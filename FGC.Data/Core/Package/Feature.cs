﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData
{
    [Table("Feature")]
    public class Feature : CoreBaseEntity
    {
        public Feature()
        {
            //FeatureModule = new FeatureModule();
        }

        public int ID { get; set; }

        [Required]
        public int ModuleID { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        public int MenuID { get; set; }
        public int FGCPortalID { get; set; }
        public int ServiceID { get; set; }
        public int FeatureParent { get; set; }

        [Required]
        public decimal Price { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        public string ValueDataType { get; set; }  

        [Required]
        public bool IsCustomizable { get; set; }    // If Yes, then charges can be modified (eg. low charges for VIP Clients)

        /// <summary>
        /// DefaultValue: It will be as per Value Data Type, Like Email Campaign Availble it is boolean i.e. Yes/No, 
        /// For No of Emails Allowed it is integer i.e. 5000... and so on
        /// </summary>
        public string DefaultValue { get; set; }    // 


        

        //public virtual FeatureModule FeatureModule { get; set; }

    }
}
