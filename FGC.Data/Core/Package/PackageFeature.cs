﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData
{
    [Table("PackageFeature")]
    public class PackageFeature: CoreBaseEntity
    {
        public PackageFeature()
        {
            //Package = new Package();
            //Feature = new Feature();
        }

        public int ID { get; set; }

        [Required]
        public int PackageID { get; set; }

        [Required]
        public int FeatureID { get; set; }

        public bool IsAvailable { get; set; }
        public bool IsOptIn { get; set; }

        //public virtual Package Package { get; set; }
        //public virtual Feature Feature { get; set; }

    }
}
