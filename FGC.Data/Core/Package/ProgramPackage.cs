﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData 
{
    [Table("ProgramPackage")]
    public class ProgramPackage: CoreBaseEntity
    {
        public ProgramPackage()
        {
            //Package = new Package();
            //Program = new Program();
        }

        public int ID { get; set; }

        [Required]
        public int PackageID { get; set; }

        [Required]
        public int ProgramID { get; set; }


        //public virtual Package Package { get; set; }
        //public virtual Program Program { get; set; }
    }
}
