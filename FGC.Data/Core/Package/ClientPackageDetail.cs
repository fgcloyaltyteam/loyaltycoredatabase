﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData
{
    [Table("ClientPackageDetail")]
    public class ClientPackageDetail : CoreBaseEntity
    {
        public ClientPackageDetail()
        {
            
        }

        public int ID { get; set; }

        [Required]
        public int ClientID { get; set; }

        [Required]
        public int ClientPackageID { get; set; }

        [Required]
        public int FeatureID { get; set; }
        public string FeatureValue { get; set; }    // Default Values will be shown from Feature Class with appropriate Value Data Type
 
    }
}
