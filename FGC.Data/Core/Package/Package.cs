﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData
{
    [Table("Package")]
    public class Package : CoreBaseEntity
    {
        public int ID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }
        public int ServiceID { get; set; }
        public int PackagePortal { get; set; }
        public int PackageTechnology { get; set; }

        //[Required]
        //public decimal Charges { get; set; }   // THis will be included in Feature as Fixed Recurring Charges as per Billing Cycle




    }
}
