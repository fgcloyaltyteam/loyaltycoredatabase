﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData
{
    [Table("ClientPackage")]
    public class ClientPackage : CoreBaseEntity
    {
        public ClientPackage()
        { 
        }

        public int ID { get; set; }

        [Required]
        public int ClientID { get; set; }

        [Required]
        public int PackageID { get; set; }

        //[Required]
        //public int FeatureID { get; set; }

        //[Required]
        //public string FeatureValue { get; set; }    // Default Values will be shown from Feature Class with appropriate Value Data Type

        [Required]
        public int BillingCycleID { get; set; }



        //public virtual GlobalUser GlobalUser { get; set; }
        //public virtual Package Package { get; set; }
        //public virtual Feature Feature { get; set; }
        //public virtual BillingCycle BillingCycle { get; set; }
    }
}
