﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData
{
    [Table("CSSRepositoryThemeVariableValue")]
    public class CSSRepositoryThemeVariableValue: BaseEntity
    {
        public CSSRepositoryThemeVariableValue()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int ThemeID { get; set; }

        [Required]
        public int VariableID { get; set; }

        [Required, MaxLength(255)]
        public string Value { get; set; }

    }
}
