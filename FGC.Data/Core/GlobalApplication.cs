﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;


namespace FGC.Data.Core
{
    public class GlobalApplication : BaseEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Guid ApplicationID { get; set; } 

        [ForeignKey("GlobalServer")]
        public int ServerID { get; set; } 
        public virtual GlobalServer GlobalServer { get; set; }

        //public virtual Department Department { get; set; }
        //public virtual ICollection<Enrollment> Enrollments { get; set; }
        //public virtual ICollection<Instructor> Instructors { get; set; }
    }
}