﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("Service")]
    public class Service : BaseEntity
    {
        public int ID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }
        public int ServiceTypeID { get; set; }

    }
}
