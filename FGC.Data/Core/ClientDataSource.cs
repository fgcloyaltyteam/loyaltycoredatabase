﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("ClientDataSource")]
    public class ClientDataSource
    {
        public ClientDataSource()
        {
        }

        [Key]
        public int ID { get; set; }

        public int DataSourceID { get; set; }
        public int GlobalServerID { get; set; }
        public string Credentials { get; set; }
        public string Account { get; set; }
    }
}