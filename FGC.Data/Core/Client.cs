﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data.Base;

namespace FGC.Data.Core
{
    public class Client : BaseEntity
    {
        
        public int ID { get; set; }
        public string CompanyName { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactMiddleName { get; set; }
        public string ContactLastName { get; set; }
        public string Email { get; set; }
        public string WebsiteURL { get; set; }
        public string RegistrationDate { get; set; }
        public int ClientStatusId { get; set; }
    }
}
