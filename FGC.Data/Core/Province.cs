﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FGC.CoreData
{
    [Table("Province")]
    public class Province : BaseEntityCore
    {
        public Province()
        {
            Country = new Country();
        }

        public int ID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [Required, MaxLength(5)]
        public string Code { get; set; }

        [Required, ForeignKey("Country")]
        public int CountryID { get; set; }




        public virtual Country Country { get; set; }
    }
}
