﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData
{
    [Table("CSSWebsiteTheme")]
    public class CSSWebsiteTheme : BaseEntity
    {
        public CSSWebsiteTheme()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required]
        public int ThemeID { get; set; }

        [Required]
        public int WebsiteID { get; set; }

        [Required, MaxLength(100)]
        public string UserThemeName { get; set; }

    }
}
