﻿using FGC.Data.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FGC.CoreData
{
    [Table("CSSLayout")]
    public class CSSLayout : BaseEntity
    {
        public CSSLayout()
        {

        }

        [Key]
        public int ID { get; set; }

        [Required, MaxLength(100)]
        public string LayoutName { get; set; }

        [Required, MaxLength(100)]
        public string LayoutImage { get; set; }

        [Required]
        public string CSSContent { get; set; }

    }
}