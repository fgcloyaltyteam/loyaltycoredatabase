﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FGC.Data.Base;
using System.Collections.Generic;

namespace FGC.CoreData
{
    [Table("ProgramWebsite")]
    public class ProgramWebsite : BaseEntity
    {
        public ProgramWebsite()
        {

        }

        [Key]
        public int ID { get; set; }

        public string SiteUrl { get; set; }

        public int PortalID { get; set; }
        public int ResourceID { get; set; }

        [Required]
        public int GlobalUserID { get; set; }

        [Required] 
        public int ProgramID { get; set; }

         
    }
}

