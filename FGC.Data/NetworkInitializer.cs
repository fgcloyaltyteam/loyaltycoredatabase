﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.CoreData;
using Newtonsoft.Json;
using System.IO;

namespace FGC.Data
{
    public static class NetworkInitializer
    {
        /* public static string GetCompanyName(int UserID)
         {
             NetWorkHandlerService _ctx = new NetWorkHandlerService();
             return _ctx.GetCompanyName(UserID);
         }*/

        //public static Guid GetSchema(int UserID)
        //{
        //    NetworkHandlerService _ctx = new NetworkHandlerService();
        //    return _ctx.GetNetworkSchema(UserID);
        //}
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["LoyaltyDBContext"].ConnectionString;
        }
        public static int DefaultPageSize()
        {
            return 25;

        }
        public static void NetworkContextInitilizer(CoreContext context)
        {
            try
            {

                //ConfigureApplicationEntitiesAndClaims(context);
                //BuildServersNetwork(context);
                //BuildCurrencyData(context);
                //BuildDefaultData(context);
                //BuildNetwokServicesPlan(context);
                //ConfigureConnecterEntities(context);
                //ConfigureConnecterTables(context);
                //BuildEmailTemplate(context);
                BuildClientDataSource(context);
               // BuildClientPackage(context);
               // BuildClientPackageDetail(context);
                BuildCSSLayout(context);
                BuildCSSTheme(context);
                BuildCSSThemeVariableValue(context);
                BuildCSSVariable(context);
                BuildCSSVariableDefaultValue(context);
                //BuildDataSources(context);
               // BuildFeature(context);
               // BuildFeatureModule(context);
               // BuildGlobalUser(context);
                //BuildPackage(context);
               // BuildPackageFeature(context);
               // BuildPortal(context);
                BuildPrograms(context);
              //  BuildProgramPackage(context);
               // BuildProgramWebsite(context);
                BuildServerTypes(context);
            }
            catch (Exception ex)
            {
               throw ex;
            }

        }
        private static void BuildPrograms(CoreContext context)
        {
            //List<Program> lstPrograms = new List<Program> {
            //    new Core.Program{ Name = "B2B", Description= "Business 2 business" },
            //    new Program {Name = "B2C", Description= "Business 2 Consumer" },
            //    new Program {Name = "B2E", Description= "Business 2 employee"}
            //};

            List<Program> lstPrograms = JsonConvert.DeserializeObject<List<Program>>(
               File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\Program.json"));

            //string dir = File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\Program.json";

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.Program.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildClientDataSource(CoreContext context)
        { 
            List<ClientDataSource> lstPrograms = JsonConvert.DeserializeObject<List<ClientDataSource>>(
               File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\ClientDataSource.json"));
            

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.ClientDataSource.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildClientPackage(CoreContext context)
        {
            List<ClientPackage> lstPrograms = JsonConvert.DeserializeObject<List<ClientPackage>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\ClientPackage.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.ClientPackage.Add(item);
            }

            context.SaveChanges();

        }

        private static void BuildClientPackageDetail(CoreContext context)
        {
            List<ClientPackageDetail> lstPrograms = JsonConvert.DeserializeObject<List<ClientPackageDetail>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\ClientPackageDetail.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.ClientPackageDetail.Add(item);
            }

            context.SaveChanges();

        }

        private static void BuildCSSLayout(CoreContext context)
        {
            List<CSSLayout> lstPrograms = JsonConvert.DeserializeObject<List<CSSLayout>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\CSSLayout.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.CSSLayout.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildCSSTheme(CoreContext context)
        {
            List<CSSTheme> lstPrograms = JsonConvert.DeserializeObject<List<CSSTheme>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\CSSTheme.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.CSSTheme.Add(item);
            }

            context.SaveChanges();

        }

        private static void BuildCSSThemeVariableValue(CoreContext context)
        {
            List<CSSThemeVariableValue> lstPrograms = JsonConvert.DeserializeObject<List<CSSThemeVariableValue>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\CSSThemeVariableValue.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.CSSThemeVariableValue.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildCSSVariable(CoreContext context)
        {
            List<CSSVariable> lstPrograms = JsonConvert.DeserializeObject<List<CSSVariable>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\CSSVariable.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.CSSVariable.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildCSSVariableDefaultValue(CoreContext context)
        {
            List<CSSVariableDefaultValue> lstPrograms = JsonConvert.DeserializeObject<List<CSSVariableDefaultValue>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\CSSVariableDefaultValue.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.CSSVariableDefaultValue.Add(item);
            }

            context.SaveChanges();

        }

        private static void BuildFeature(CoreContext context)
        {
            List<Feature> lstPrograms = JsonConvert.DeserializeObject<List<Feature>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\Feature.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.Feature.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildFeatureModule(CoreContext context)
        {
            List<FeatureModule> lstPrograms = JsonConvert.DeserializeObject<List<FeatureModule>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\FeatureModule.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.FeatureModule.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildGlobalUser(CoreContext context)
        {
            List<GlobalUser> lstPrograms = JsonConvert.DeserializeObject<List<GlobalUser>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\GlobalUser.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.GlobalUser.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildPackage(CoreContext context)
        {
            List<Package> lstPrograms = JsonConvert.DeserializeObject<List<Package>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\Package.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.Package.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildPackageFeature(CoreContext context)
        {
            List<PackageFeature> lstPrograms = JsonConvert.DeserializeObject<List<PackageFeature>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\PackageFeature.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.PackageFeature.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildPortal(CoreContext context)
        {
            List<Portal> lstPrograms = JsonConvert.DeserializeObject<List<Portal>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\Portal.json"));

            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.Portal.Add(item);
            }

            context.SaveChanges();

        }
        //private static void BuildProgramPackage(CoreContext context)
        //{
        //    List<ProgramPackage> lstProgram = JsonConvert.DeserializeObject<List<ProgramPackage>>(
        //        File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\ProgramPackage.json"));

        //    foreach (var item in lstProgram)
        //    {
        //        context.Entry(item).State = System.Data.Entity.EntityState.Added;
        //        context.ProgramPackage.Add(item);
        //    }

        //    context.SaveChanges();

        //}
        private static void BuildProgramWebsite(CoreContext context)
        {
            List<ProgramWebsite> lstProgram = JsonConvert.DeserializeObject<List<ProgramWebsite>>(
                File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\ProgramWebsite.json"));

            foreach (var item in lstProgram)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.ProgramWebsite.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildDataSources(CoreContext context)
        {
            //List<DataSource> lstPrograms = new List<DataSource>
            //{
            //    new  DataSource{ Name = "Volusion 1.0", ApiName = "Volusion", CredentialTemplate = "Template", Credentials = "Credentials"}
            //};

            List<DataSource> lstPrograms = JsonConvert.DeserializeObject<List<DataSource>>(File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\DataSource.json"));



            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.DataSource.Add(item);
            }

            context.SaveChanges();

        }

        private static void BuildServerTypes(CoreContext context)
        {
            //List<ServerType> lstPrograms = new List<ServerType> {
            //    new  ServerType{ Name = "Database Server"},
            //    new  ServerType{ Name = "Connector server"},
            //};
            List<ServerType> lstPrograms = JsonConvert.DeserializeObject<List<ServerType>>(File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\ServerType.json"));



            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.ServerType.Add(item);
            }

            context.SaveChanges();

        }
        private static void BuildSuperuser(CoreContext context)
        {
            //List<Program> lstPrograms = new List<Program> {
            //    new Core.Program{ Name = "B2B", Description= "Business 2 business" },
            //    new Program {Name = "B2C", Description= "Business 2 Consumer" },
            //    new Program {Name = "B2E", Description= "Business 2 employee"}
            //};
            List<Program> lstPrograms = JsonConvert.DeserializeObject<List<Program>>(File.ReadAllText(Environment.CurrentDirectory + @"\CoreDbJson\Program.json"));


            foreach (var item in lstPrograms)
            {
                context.Entry(item).State = System.Data.Entity.EntityState.Added;
                context.Program.Add(item);
            }

            context.SaveChanges();

        }

        private static void BuildServersNetwork(CoreContext context)
        {
            var globalserver = new GlobalServer();
            globalserver.IPAddress = "1.1.1.1";
            globalserver.Name = "DemoServer";
            globalserver.Priority = 1;

            //globalserver.GlobalUser = new GlobalUser { };

            globalserver.UserSchemaID = Guid.NewGuid();
            context.Entry(globalserver).State = System.Data.Entity.EntityState.Added;
            context.GlobalServer.Add(globalserver);
            context.SaveChanges();

        }
       
    }
}
