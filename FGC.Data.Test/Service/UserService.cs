﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FGC.Data;
using FGC.CoreData;

namespace FGC.Data.Test.Service
{
    public class UserService
    {
        //private readonly IUnitOfWork _unitOfWork;

        public UserService()//IUnitOfWork iow
        { 
           // _unitOfWork = iow;
        }

        public GlobalUser CreateUser(GlobalUser user)
        {
            /*
             1- create context
             2- add object in entity
             3- set EntityState
             4- Save changes
             * 
             * Warning
             * Minimize dependencies where possible
             * Make sure navigational properties should not get saved unless required
             
             */


            using (CoreContext c = new CoreContext())
            {
                c.GlobalUser.Add(user);
                c.Entry(user).State = System.Data.Entity.EntityState.Added;
                var u = c.SaveChanges();
                return user;
            }
        }
    }
}
